package com.welldone_ural.android.model;

public class Result
{
	public Result(String r, int p)
	{
		result = r;
		price = p;
	}
	
	public String getResult()
	{
		return result;
	}
	
	public int getPrice()
	{
		return price;
	}
	
	public int getIntValue()
	{
		try
		{
			return Integer.parseInt(result);
		}
		catch(NumberFormatException e)
		{
			return -1;
		}
	}
	
	@Override
	public String toString()
	{
		return result+" price: "+price;
	}
	
	private String result;
	private int price;
	
	public static final String CACHE_KEY = "result";
}