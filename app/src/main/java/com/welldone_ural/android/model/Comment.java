package com.welldone_ural.android.model;

import com.google.gson.annotations.SerializedName;

public class Comment extends Entity
{
	public Comment()
	{
		super(0, "");
	}
	
	public String getMessage()
	{
		return message;
	}
	
	public String getDate()
	{
		return date;
	}
	
	public String getUserName()
	{
		return authorName;
	}
	
	public String getAvatarUrl()
	{
		if(authorAvatar == null || authorAvatar.equals("null")) return null;
		return URL_MEDIA_PREFIX+authorAvatar;
	}
	
	private String message;
	@SerializedName("date_add")
	private String date;
	@SerializedName("author-name")
	private String authorName;
	@SerializedName("author-avatar")
	private String authorAvatar;
	@SerializedName("author-id")
	private long authorId;
}