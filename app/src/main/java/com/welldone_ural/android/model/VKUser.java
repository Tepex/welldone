package com.welldone_ural.android.model;

import android.content.Context;
import android.content.SharedPreferences;

import android.support.annotation.Nullable;

public class VKUser extends Entity
{
	private VKUser(long uid, String n, String ph, String av)
	{
		super(uid, n);
		mobile_phone = ph;
		photo_max = av;
	}
	
	@Nullable
	public static VKUser getUser(Context context)
	{
		SharedPreferences prefs = context.getSharedPreferences(VK, Context.MODE_PRIVATE);
		long id = prefs.getLong(ID, 0);
		if(id == 0) return null;
		return new VKUser(id, prefs.getString(NAME, null), prefs.getString(User.PHONE, null), prefs.getString(User.AVATAR, null));
	}

	public void save(Context context)
	{
		SharedPreferences.Editor editor = context.getSharedPreferences(VK, Context.MODE_PRIVATE).edit();
		editor.putLong(ID, getId());
		editor.putString(NAME, getName());
		editor.putString(User.PHONE, getPhone());
		editor.putString(User.AVATAR, getAvatar());
		editor.apply();
	}
	
	@Override
	public String getName()
	{
		if(name == null) name = first_name+" "+last_name;
		return name;
	}
	
	@Override
	public String toString()
	{
		return "VKUser: name: "+getName()+" id: "+getId()+" avatar: "+getAvatar();
	}
	
	public String getAvatar()
	{
		return photo_max;
	}
	
	public String getPhone()
	{
		return mobile_phone;
	}
	
	public static void remove(Context context)
	{
		SharedPreferences.Editor editor = context.getSharedPreferences(VK, Context.MODE_PRIVATE).edit();
		editor.clear();
		editor.apply();
	}
	
	private String first_name;
	private String last_name;
	private String photo_max;
	private String mobile_phone;
	
	public static final long NO_ID = -1;
	public static final String VK = "vk";
}