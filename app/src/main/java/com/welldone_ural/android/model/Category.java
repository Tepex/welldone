package com.welldone_ural.android.model;

public class Category extends Entity
{
	public Category(long id, String name)
	{
		super(id, name);
	}
	
	public Category[] getSubcategories()
	{
		return childs;
	}
	
	@Override
	public String toString()
	{
		return name;
	}
	
	private Category[] childs;
	
	static
	{
		CACHE_KEY = "categories";
	}
}