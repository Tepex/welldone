package com.welldone_ural.android.model;

public class VKUsersResponse
{
	public VKUser getUser()
	{
		if(response == null || response.length == 0) return null;
		return response[0];
	}
	
	private VKUser[] response;
}