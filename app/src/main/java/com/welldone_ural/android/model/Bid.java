package com.welldone_ural.android.model;

import android.os.Parcel;
import android.os.Parcelable;

import android.support.annotation.StringRes;

import android.text.TextUtils;

import com.welldone_ural.android.R;

import java.util.Date;

public class Bid extends Entity implements Parcelable
{
	private Bid(Parcel parcel)
	{
		super(parcel);
		title = name;
		description = parcel.readString();
		media = parcel.readString();
		weight = parcel.readInt();
		address = parcel.readString();
		date = parcel.readString();
		price = parcel.readInt();
		rating = parcel.readDouble();
		rating_count = parcel.readInt();
		status = parcel.readInt();
		
		category = parcel.readLong();
		category_name = parcel.readString();
		subcategory = parcel.readLong();
		subcategory_name = parcel.readString();
		
		id_author = parcel.readLong();
		author_name = parcel.readString();
		author_surname = parcel.readString();
		author_avatar = parcel.readString();
		
		id_executor = parcel.readLong();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		super.writeToParcel(dest, flags);
		dest.writeString(description);
		dest.writeString(media);
		dest.writeInt(weight);
		dest.writeString(address);
		dest.writeString(date);
		dest.writeInt(price);
		dest.writeDouble(rating);
		dest.writeInt(rating_count);
		dest.writeInt(status);
		
		dest.writeLong(category);
		dest.writeString(category_name);
		dest.writeLong(subcategory);
		dest.writeString(subcategory_name);
		
		dest.writeLong(id_author);
		dest.writeString(author_name);
		dest.writeString(author_surname);
		dest.writeString(author_avatar);
		
		dest.writeLong(id_executor);
	}

	@Override
	public String getName()
	{
		return title;
	}
	
	@Override
	public String toString()
	{
		return "[id:"+getId()+", title:"+getName()+", status:"+getStatus()+", price:"+getPrice()+"]";
	}
	
	public String getDescription()
	{
		return description;
	}
	
	public String getImageUri()
	{
		return URL_MEDIA_PREFIX+media;
	}
	
	public int getWeight()
	{
		return weight;
	}
	
	public String getAddress()
	{
		if(TextUtils.isEmpty(address)) return null;
		return address;
	}
	
	public String getDate()
	{
		return date;
	}
	
	public int getPrice()
	{
		return price;
	}
	
	public void setPrice(int p)
	{
		price = p;
	}
	
	public double getRating()
	{
		return rating;
	}
	
	public int getRatingCount()
	{
		return rating_count;
	}
	
	public long getCategoryId()
	{
		return category;
	}
	
	public String getCategoryName()
	{
		return category_name;
	}
	
	public long getSubcategoryId()
	{
		return subcategory;
	}
	
	public String getSubcategoryName()
	{
		return subcategory_name;
	}
	
	public long getUserId()
	{
		return id_author;
	}
	
	public String getUserName()
	{
		return author_name+" "+author_surname;
	}
	
	public String getUserAvatar()
	{
		if(author_avatar == null || author_avatar.equals("null") || author_avatar.equals("-1")) return null;
		return URL_MEDIA_PREFIX+author_avatar;
	}
	
	public long getExecutorId()
	{
		return id_executor;
	}
	
	public int getStatus()
	{
		return status;
	}
	
	public @StringRes int getStatusRes()
	{
		return STATUS[status];
	}
	
	protected String title;
	protected String description;
	protected String media;
	protected int weight;
	protected String address;
	//protected Date date;
	protected String date;
	protected int price;
	protected double rating;
	protected int rating_count;
	protected int status;
	
	protected long category;
	protected String category_name;
	protected long subcategory;
	protected String subcategory_name;
	
	protected long id_author;
	protected String author_name;
	protected String author_surname;
	protected String author_avatar;
	
	protected long id_executor;
	
	public static final Parcelable.Creator CREATOR = new Parcelable.Creator()
		{
			@Override
			public Bid createFromParcel(Parcel in)
			{
				return new Bid(in);
			}
			
			@Override
			public Bid[] newArray(int size)
			{
				return new Bid[size];
			}
		};
		
	private static final int[] STATUS = {
		R.string.bid_not_viewed,
		R.string.bid_accepted,
		R.string.bid_discarded,
		R.string.bid_in_progress,
		R.string.bid_completed};
	
	static
	{
		CACHE_KEY = "bids";
	}
	
	public static String CACHE_KEY_IN_WAIT = "bids_in_wait";
	public static String CACHE_KEY_INCOMPLETE = "bids_incomplete";
	public static String CACHE_KEY_IN_WORK = "bids_in_work";
}