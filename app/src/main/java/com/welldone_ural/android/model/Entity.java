package com.welldone_ural.android.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.CallSuper;

public class Entity implements Parcelable
{
	protected Entity(Parcel parcel)
	{
		id = parcel.readLong();
		name = parcel.readString();
	}
	
	@CallSuper
	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		dest.writeLong(getId());
		dest.writeString(getName());
	}
	
	@Override
	public int describeContents()
	{
		return 0;
	}
	
	protected Entity(long id, String name)
	{
		this.id = id;
		this.name = name;
	}
	
	public long getId()
	{
		return id;
	}
	
	public String getName()
	{
		return name;
	}
	
	public int getResult()
	{
		return result;
	}
	
	@Override
	public String toString()
	{
		return name+" ["+id+"]";
	}
	
	@Override
	public int hashCode()
	{
		return (int)id;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj == null) return false;
		if(!(obj instanceof Entity)) return false;
		Entity other = (Entity)obj;
		return id == other.id;
	}
	
	protected long id;
	protected String name;
	protected int result;
	
	protected long userID;
	
	public static final String ID = "id";
	public static final String NAME = "name";

	//public static final String URL_MEDIA_PREFIX = "http://welldone.eduqe.com/media/";
	public static final String URL_MEDIA_PREFIX = "http://welldone-ural.ru/media/";
	
	public static String CACHE_KEY;
	public static Parcelable.Creator CREATOR;
}