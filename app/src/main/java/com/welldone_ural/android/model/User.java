package com.welldone_ural.android.model;

import android.content.Context;
import android.content.SharedPreferences;

import android.os.Parcel;
import android.os.Parcelable;

import android.support.annotation.Nullable;

import android.text.TextUtils;

import android.util.Log;

import com.welldone_ural.android.BuildConfig;

import java.util.prefs.Preferences;
import static com.welldone_ural.android.Utils.TAG;

public class User extends Entity
{
	public User(long uid, String uname, String uphone, String uemail, String upwd, String uavatar, String uabout, int ubalance, int ustatus, boolean uloggedIn)
	{
		super(uid, uname);
		phone = uphone;
		email = uemail;
		pwd = upwd;
		avatar = uavatar;
		about = uabout;
		balance = ubalance;
		status = ustatus;
		loggedIn = uloggedIn;
	}
	
	public User(Parcel parcel)
	{
		super(parcel);
		avatar = parcel.readString();
		rating = parcel.readDouble();
		count = parcel.readInt();
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		super.writeToParcel(dest, flags);
		dest.writeString(getAvatar());
		dest.writeDouble(getRating());
		dest.writeInt(getCount());
	}
	
	
	@Nullable
	public static User getUser(Context context)
	{
		SharedPreferences prefs = context.getSharedPreferences(SP, Context.MODE_PRIVATE);
		long id = prefs.getLong(ID, -88);
		if(id == 0) return null;
		return new User(
			id,
			prefs.getString(NAME, null),
			prefs.getString(PHONE, null),
			prefs.getString(EMAIL, null),
			prefs.getString(PWD, null),
			prefs.getString(AVATAR, null),
			prefs.getString(ABOUT, null),
			prefs.getInt(BALANCE, 0),
			prefs.getInt(STATUS, 0),
			prefs.getBoolean(LOGGED_IN, false));
	}
	
	public void save(Context context)
	{
		SharedPreferences.Editor editor = context.getSharedPreferences(SP, Context.MODE_PRIVATE).edit();
		if(BuildConfig.DEBUG) Log.d(TAG, "saving user id="+getId()+" logged in: "+isLoggedIn());
		
		editor.putLong(ID, getId());
		editor.putString(NAME, getName());
		editor.putString(PHONE, getPhone());
		editor.putString(EMAIL, getEmail());
		editor.putString(PWD, getPwd());
		editor.putString(AVATAR, getAvatar());
		editor.putString(ABOUT, getAbout());
		editor.putInt(BALANCE, getBalance());
		editor.putInt(STATUS, getStatus());
		editor.putBoolean(LOGGED_IN, isLoggedIn());
		editor.apply();
		
		if(BuildConfig.DEBUG) Log.d(TAG, "id after save : "+context.getSharedPreferences(SP, Context.MODE_PRIVATE).getLong(ID, -9));
		
	}
	
	public void remove(Context context)
	{
		SharedPreferences.Editor editor = context.getSharedPreferences(SP, Context.MODE_PRIVATE).edit();
		editor.clear();
		editor.apply();
	}
	
	public void update(User newUser)
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "update new user: "+newUser);
		id = newUser.getId();
		name = newUser.getName();
		phone = newUser.getPhone();
		email = newUser.getEmail();
		setAvatar(newUser.getAvatar());
		about = newUser.getAbout();
		balance = newUser.getBalance();
		status = newUser.getStatus();
		loggedIn = true;
		
		
		//save(context);
	}
	
	@Override
	public String toString()
	{
		return "[result: "+getResult()+", id: "+getId()+", name: "+getName()+", phone: "+getPhone()+", email: "+getEmail()+", pwd: "+getPwd()+", about: "+getAbout()+", rate: "+getRate()+", balance: "+getBalance()+", positive: "+getPositiveFeedbackCount()+" negative: "+getNegativeFeedbackCount()+", bids count: "+getBidsCount()+", isExecutor: "+isExecutor()+" isLoggedIn: "+isLoggedIn()+"]";
	}
	
	@Override
	public long getId()
	{
		if(result > 0) id = result;
		return id;
	}
	
	public String getPhone()
	{
		return phone;
	}
	
	public String getEmail()
	{
		return email;
	}
	
	public String getPwd()
	{
		return pwd;
	}
	
	public void setPwd(String p)
	{
		pwd = p;
	}
	
	public String getAvatar()
	{
		return avatar;
	}
	
	public void setAvatar(String av)
	{
		avatar = av;
		if(avatar == null) avatarUrl = null;
		else
		{
			if(BuildConfig.DEBUG) Log.d(TAG, "User.setAvatar avatar="+avatar);
			
			avatarUrl = URL_MEDIA_PREFIX+avatar;
		}
	}
	
	public String getAvatarUrl()
	{
		if(avatarUrl == null)
		{
			if(avatar != null && !TextUtils.isEmpty(avatar) && !avatar.equals("null")) avatarUrl = URL_MEDIA_PREFIX+avatar;
		}
		if(BuildConfig.DEBUG) Log.d(TAG, "User("+id+").avatarUrl="+avatarUrl);
		return avatarUrl;
	}
	
	public String getAbout()
	{
		return about;
	}
	
	public int getBalance()
	{
		return balance;
	}
	
	public int getStatus()
	{
		return status;
	}
	
	public boolean isExecutor()
	{
		return status > 0;
	}
	
	public int getRate()
	{
		return rate;
	}
	
	public int getPositiveFeedbackCount()
	{
		if(true) return 44;
		return positiveFeedbackCount;
	}
	
	public int getNegativeFeedbackCount()
	{
		return negativeFeedbackCount;
	}
	
	public int getBidsCount()
	{
		bidsCount = 33;
		return bidsCount;
	}
	
	public boolean isLoggedIn()
	{
		return loggedIn;
	}
	
	public void setLoggedIn(boolean l)
	{
		loggedIn = l;
	}
	
	public double getRating()
	{
		return rating;
	}
	
	public int getCount()
	{
		return count;
	}
	
	private String email;
	private String pwd;
	private String phone;
	private String avatar;
	private String about;
	private int balance;
	private int status;
	private int rate = 77;
	private int bidsCount;
	
	/* исполнители */
	private double rating;
	private int count;
	
	private String avatarUrl;
	private boolean loggedIn;
	
	private int positiveFeedbackCount = 5;
	private int negativeFeedbackCount = 7;
	
	public static final String SP = "SP";
	
	public static final String PHONE = "phone";
	public static final String EMAIL = "email";
	public static final String PWD = "pwd";
	public static final String AVATAR = "avatar";
	public static final String ABOUT = "about";
	public static final String STATUS = "status";
	public static final String BALANCE = "balance";
	public static final String LOGGED_IN = "logged_in";
	
	public static final int ERR_AUTH = -1;
	public static final int ERR_OTHER = 0;
	public static final int ERR_EMAIL = -1;
	public static final int ERR_EMAIL_ALLREADY_EXISTS = -2;
	public static final int ERR_PHONE_ALLREADY_EXISTS = -3;
	
	static
	{
		CACHE_KEY = "user";
	}
	
	public static final String CACHE_KEY_EXECUTOR = "executor";
	
	public static final Parcelable.Creator CREATOR = new Parcelable.Creator()
	{
		@Override
		public User createFromParcel(Parcel in)
		{
			return new User(in);
		}
			
		@Override
		public User[] newArray(int size)
		{
			return new User[size];
		}
	};
}