package com.welldone_ural.android.ui;

import android.app.Activity;

import android.content.Context;

import android.graphics.Color;

import android.support.annotation.StringRes;

import android.util.Log;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import com.welldone_ural.android.BuildConfig;
import com.welldone_ural.android.model.Bid;
import com.welldone_ural.android.R;

import static com.welldone_ural.android.Utils.TAG;

import java.util.ArrayList;
import java.util.List;

public class BidAdapter extends ArrayAdapter<Bid>
{
	public BidAdapter(Context context, Bid[] bids)
	{
		super(context, R.layout.bid_row, bids);
		this.context = context;
		inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public long getItemId(int pos)
	{
		return getItem(pos).getId();
	}
	
	@Override
	public View getView(int pos, View convertView, ViewGroup parent)
	{
		ViewHolder holder;
		if(convertView == null)
		{
			convertView = inflater.inflate(R.layout.bid_row, parent, false);
			holder = new ViewHolder();
			holder.authorNameView = (TextView)convertView.findViewById(R.id.bid_author_name);
			holder.titleView = (TextView)convertView.findViewById(R.id.bid_title);
			holder.statusView = (TextView)convertView.findViewById(R.id.bid_status);
			holder.priceView = (TextView)convertView.findViewById(R.id.bid_price);
			holder.imageView = (ImageView)convertView.findViewById(R.id.avatar);
			convertView.setTag(holder);
		}
		else holder = (ViewHolder)convertView.getTag();
		/*
		if(pos % 2 == 0) convertView.setBackgroundColor(Color.GRAY);
		else convertView.setBackgroundColor(Color.TRANSPARENT);
		*/
		Bid item = getItem(pos);
		holder.authorNameView.setText(item.getUserName());
		String title = item.getName();
		holder.titleView.setText(title);
		holder.statusView.setText(context.getString(item.getStatusRes()));
		if(item.getPrice() > 0) holder.priceView.setText(context.getString(R.string.bid_price, item.getPrice()));
		else holder.priceView.setText("");
		
		Picasso picasso = Picasso.with(context);
		RequestCreator rc;
		if(item.getUserAvatar() == null) rc = picasso.load(R.drawable.ic_avatar_none);
		else rc = picasso.load(item.getUserAvatar());
		rc.resize(60, 60).centerCrop().into(holder.imageView);
		
		return convertView;
	}
	
	private Context context;
	private LayoutInflater inflater;
	
	static class ViewHolder
	{
		TextView authorNameView;
		TextView titleView;
		TextView statusView;
		TextView priceView;
		ImageView imageView;
	}
}
