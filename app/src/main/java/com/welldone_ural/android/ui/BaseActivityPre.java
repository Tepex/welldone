package com.welldone_ural.android.ui;

import android.app.ProgressDialog;

import android.content.DialogInterface;
import android.content.Intent;

import android.os.Bundle;

import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.util.Log;

import android.view.MenuItem;

import android.widget.Spinner;

import com.welldone_ural.android.BuildConfig;
import com.welldone_ural.android.controller.BaseController;
import com.welldone_ural.android.R;

import static com.welldone_ural.android.Utils.TAG;

/**
 * Одно Activity для всех операций.
 */
public class BaseActivityPre extends AppCompatActivity implements CompatibleActivity
{
	@CallSuper
	@Override
	protected void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		onCreate(this, bundle);
	}
	
	@Override
	public @LayoutRes int getLayout()
	{
		return R.layout.base_layout;
	}
	
	@Override
	public void initControls()
	{
		toolbar = (Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
	}
	
	@Override
	public void showToolbar(boolean visible, String title)
	{
		if(visible)
		{
			getSupportActionBar().show();
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
			getSupportActionBar().setTitle(title);
		}
		else getSupportActionBar().hide();
	}
	
	@Override
	public void setTitle(int titleId)
	{
		getSupportActionBar().setTitle(titleId);
	}
	
	@CallSuper
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		return isUpButtonPressed(this, item);
	}
	
	@CallSuper
	@Override
	public void onBackPressed()
	{
		if(!isExit(this)) super.onBackPressed();
	}
	
	@CallSuper
	@Override
	public void onSaveInstanceState(Bundle bundle)
	{
		saveCurrentFragment(this, bundle);
		super.onSaveInstanceState(bundle);
	}
	
	@CallSuper
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		BaseController controller = (BaseController)CompatibleActivity.getCurrentFragment(getSupportFragmentManager());
		if(controller != null) controller.onActivityResult(requestCode, resultCode, data);
	}
	
	@Override
	public void showProgressDialog(@StringRes int title, DialogInterface.OnCancelListener listener)
	{
		cancelProgressDialog();
		progressDialog = ProgressDialog.show(this, getString(title), getString(R.string.progress_wait), true, true, listener);
	}
	
	@Override
	public void cancelProgressDialog()
	{
		if(progressDialog != null)
		{
			progressDialog.dismiss();
			progressDialog = null;
		}
	}
	
	@Override
	public void showAlertDialog(@StringRes int title, String msg)
	{
		cancelProgressDialog();
		alertDialog = new AlertDialog.Builder(this)
			.setMessage(msg)
			.setTitle(title)
			.setPositiveButton(R.string.ok, (DialogInterface dialog, int id)->alertDialog.dismiss())
			.create();
		alertDialog.show();
	}
	
	protected Toolbar toolbar;
	protected ProgressDialog progressDialog;
	protected AlertDialog alertDialog;
}