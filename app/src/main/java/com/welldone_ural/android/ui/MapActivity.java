package com.welldone_ural.android.ui;

import android.content.Intent;

import android.location.Address;
import android.location.Geocoder;

import android.os.Bundle;

import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.StringRes;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.util.Log;

import android.view.MenuItem;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import com.welldone_ural.android.controller.BaseController;
import com.welldone_ural.android.controller.BidController;
import com.welldone_ural.android.controller.LastBidsController;

import com.welldone_ural.android.model.Bid;

import com.welldone_ural.android.BuildConfig;
import com.welldone_ural.android.R;

import java.util.List;

import static com.welldone_ural.android.Utils.TAG;

public class MapActivity extends AppCompatActivity
{
	@Override
	protected void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		setContentView(R.layout.map_layout);
		
		Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		map = ((MapFragment)getFragmentManager().findFragmentById(R.id.map)).getMap();
		
		if(bundle == null) bundle = getIntent().getExtras();
		bid = (Bid)bundle.getParcelable(BidController.ARG_BID);
		if(BuildConfig.DEBUG) Log.d(TAG, "Map address: "+bid);
		getSupportActionBar().setTitle(bid.getName());
		
		Geocoder geocoder = new Geocoder(this);  
		List<Address> addresses;
		try
		{
			addresses = geocoder.getFromLocationName(bid.getAddress(), 1);
		}
		catch(Exception e)
		{
			if(BuildConfig.DEBUG) Log.e(TAG, "Google maps error", e);
			finish();
			return;
		}
		if(addresses.size() > 0)
		{
			if(BuildConfig.DEBUG) Log.d(TAG, "address "+addresses.get(0));
			LatLng ll = new LatLng(addresses.get(0).getLatitude(), addresses.get(0).getLongitude());
			Marker hamburg = map.addMarker(new MarkerOptions().position(ll).title(bid.getAddress()));			
			map.moveCamera(CameraUpdateFactory.newLatLngZoom(ll, 15));
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if(item.getItemId() != android.R.id.home) return false;
		onBackPressed();
		return true;
	}
	
	@Override
	public void onBackPressed()
	{
		Bundle args = new Bundle();
		args.putParcelable(BidController.ARG_BID, bid);
		args.putString(BaseController.ARG_PARENT, LastBidsController.class.getName());
		CompatibleActivity.goBaseActivity(this, BidController.class.getName(), args);
	}	
	
	private Bid bid;
	private GoogleMap map;
}