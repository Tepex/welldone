package com.welldone_ural.android.ui;

import android.app.Activity;

import android.content.Context;
import android.content.Intent;

import android.os.Build;
import android.os.Bundle;

import android.support.annotation.LayoutRes;
import android.support.v4.app.FragmentManager;

import android.util.Log;

import com.welldone_ural.android.BuildConfig;
import com.welldone_ural.android.R;
import com.welldone_ural.android.Utils;

import com.welldone_ural.android.controller.BaseController;
import com.welldone_ural.android.controller.LoginController;
import com.welldone_ural.android.controller.UserNewController;
import com.welldone_ural.android.model.User;

import static com.welldone_ural.android.Utils.TAG;

/**
 * Редирект на нужную версию ОС.
*/
public class StartActivity extends Activity
{
	@Override
	protected void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		
		if(BuildConfig.DEBUG) FragmentManager.enableDebugLogging(true);
		
		Class activityClass = BaseActivityPre.class;
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) activityClass = BaseActivityLP.class;
		String controllerClassName;
		
		User user = User.getUser(this);
		if(BuildConfig.DEBUG) Log.d(TAG, "StartActivity User: "+user);
		
		if(user == null || user.getId() <= 0) controllerClassName = UserNewController.class.getName();
		else
		{
			if(!user.isLoggedIn()) controllerClassName = LoginController.class.getName();
			else
			{
				CompatibleActivity.goMainActivity(this, null);
				return;
			}
		}
		if(BuildConfig.DEBUG) Log.d(TAG, "new Controller: "+controllerClassName);
		
		bundle = new Bundle();
		bundle.putString(CompatibleActivity.CONTROLLER_CLASS_NAME, controllerClassName);
		Intent intent = new Intent(this, activityClass);
		intent.putExtras(bundle);
		startActivity(intent);
		finish();
	}		
}
