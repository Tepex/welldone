package com.welldone_ural.android.ui;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.vk.sdk.VKSdk;

import com.welldone_ural.android.model.User;
import com.welldone_ural.android.model.VKUser;

import com.welldone_ural.android.BuildConfig;

import static com.welldone_ural.android.Utils.TAG;

public class WellDoneApplication extends Application
{
	@Override
	public void onCreate()
	{
		super.onCreate();
		VKSdk.initialize(this);
	}
	
	public static User getUser()
	{
		return user;
	}
	
	public static VKUser getVKUser()
	{
		return vkUser;
	}
	
	public static void setUser(User u)
	{
		user = u;
	}
	
	public static void setVKUser(VKUser u)
	{
		vkUser = u;
	}
	
	private static User user;
	private static VKUser vkUser;
}