package com.welldone_ural.android.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.os.Build;
import android.os.Bundle;

import android.support.annotation.LayoutRes;
import android.support.annotation.StringRes;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;

import android.util.Log;

import android.view.Menu;
import android.view.MenuItem;

import android.widget.Spinner;

import com.welldone_ural.android.BuildConfig;
import com.welldone_ural.android.controller.AllExecutorsController;
import com.welldone_ural.android.controller.BaseController;
import com.welldone_ural.android.controller.LastBidsController;
import com.welldone_ural.android.model.User;
import com.welldone_ural.android.R;
import com.welldone_ural.android.Utils;

import java.lang.reflect.Constructor;

import static com.welldone_ural.android.Utils.TAG;


/**
transparent

http://www.androiddesignpatterns.com/2014/12/activity-fragment-transitions-in-android-lollipop-part1.html
*/

public interface CompatibleActivity
{
	default void onCreate(final FragmentActivity activity, Bundle savedInstanceState)
	{
		activity.setContentView(getLayout());
		if(BuildConfig.DEBUG) Log.d(TAG, "ActivityBase.onCreate("+savedInstanceState+")");
		initControls();
		final FragmentManager fragmentManager = activity.getSupportFragmentManager();		
		fragmentManager.addOnBackStackChangedListener(()->
		{
			Fragment fragment = getCurrentFragment(fragmentManager);
			if(BuildConfig.DEBUG) Log.d(TAG, "My back stack listener: "+fragment);
			
			if(fragment == null) return;
			boolean hasToolbar = true;
			String title = "WellDone";
			if(fragment instanceof BaseController)
			{
				BaseController c = (BaseController)fragment;
				hasToolbar = c.hasToolbar();
				title = c.getTitle();
				showToolbar(hasToolbar, title);
			}
		});
		
		final BaseController controller;
		if(savedInstanceState != null)
		{
			String tag = savedInstanceState.getString(CONTROLLER_CLASS_NAME);
			if(BuildConfig.DEBUG) Log.d(TAG, "Configuration changed. Restore controller "+tag);
			if(tag == null)
			{
				showAlertDialog(R.string.err_title, activity.getString(R.string.app_error, Utils.ERR_CONFIGURATION_CHANGED));
				activity.finish();
				System.exit(-1);
				return;
			}
			controller = (BaseController)fragmentManager.findFragmentByTag(tag);
			showToolbar(true, controller.getTitle());
		}
		else // Создание нового фрагмента
		{
			Bundle bundle = activity.getIntent().getExtras();
			String controllerClassName = bundle.getString(CONTROLLER_CLASS_NAME); 
			if(BuildConfig.DEBUG) Log.d(TAG, "new Controller: "+controllerClassName);
			try
			{
				Class<? extends BaseController> controllerClass = Class.forName(controllerClassName).asSubclass(BaseController.class);
				Constructor<? extends BaseController> constructor = (Constructor<? extends BaseController>)controllerClass.getConstructor(new Class<?>[0]);
				controller = constructor.newInstance(new Object[0]);
			}
			catch(Exception e)
			{
				showAlertDialog(R.string.err_title, activity.getString(R.string.app_error, Utils.ERR_CAST_CONTROLLER));
				if(BuildConfig.DEBUG) Log.e(TAG, "App error!", e);
				activity.finish();
				return;
			}
			
			Bundle args = bundle.getBundle(ARGS);
			if(BuildConfig.DEBUG) Log.d(TAG, "args: "+args);
			if(args != null) controller.setArguments(args);
		}
		setFragment(activity, controller, savedInstanceState == null);
	}
	
	default void saveCurrentFragment(FragmentActivity activity, Bundle bundle)
	{
		Fragment fragment = getCurrentFragment(activity.getSupportFragmentManager());
		if(BuildConfig.DEBUG) Log.d(TAG, "saving current fragment "+fragment.getClass().getName());
		if(fragment != null) bundle.putString(CONTROLLER_CLASS_NAME, fragment.getClass().getName());
	}
	
	default boolean isUpButtonPressed(FragmentActivity activity, MenuItem item)
	{
		if(item.getItemId() != android.R.id.home) return false;
		FragmentManager fragmentManager = activity.getSupportFragmentManager();
		int count = fragmentManager.getBackStackEntryCount();
		if(count == 1)
		{
			if(BuildConfig.DEBUG) Log.d(TAG, "Activity. Up button pressed. Stack count = 1");
			Fragment fragment = getCurrentFragment(fragmentManager);	
			if(fragment != null)
			{
				if(BuildConfig.DEBUG) Log.d(TAG, "Last controller in stack: "+fragment.getClass().getSimpleName());
				Bundle args = fragment.getArguments();
				if(args != null)
				{
					String parentClassName = args.getString(BaseController.ARG_PARENT);
					if(BuildConfig.DEBUG) Log.d(TAG, "parent = "+parentClassName);
					goMainActivity(activity, parentClassName);
					return true;
				}
			}
			activity.finish();
			return true;
		}
		return fragmentManager.popBackStackImmediate();
	}
	
	/**
	 * @TODO: Добавить диалог подтверждения выхода.
	 */
	default boolean isExit(Activity activity)
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "on back pressed");
		activity.finish();
		return true;
	}
	
	static void setFragment(FragmentActivity activity, BaseController controller, boolean addToBackStack)
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "setFragment "+controller+" meuID="+controller+" addToBackStack: "+addToBackStack);
		FragmentManager fragmentManager = activity.getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		String tag = controller.getClass().getName();
		fragmentTransaction.replace(R.id.content, controller, tag);
		if(addToBackStack) fragmentTransaction.addToBackStack(tag);
		fragmentTransaction.commit();
		
		 
		NavigationView navigationView = (NavigationView)activity.findViewById(R.id.navigation_view);
		if(navigationView == null) return;
		Menu menu = navigationView.getMenu();
		
		if(BuildConfig.DEBUG) Log.d(TAG, "menu size "+menu.size());
		
		/* С каждым (почти) элементом меню связан отображаемый фрагмент по действию этого меню.
		   Блокируем соответствующее меню текущего фрагмента */
		  /*
		for(int i = 0; i < menu.size(); ++i)
		{
			MenuItem menuItem = menu.getItem(i);
			menuItem.setEnabled(true);
		}
		if(controller instanceof LastBidsController) menu.getItem(1).setEnabled(false);
		else if(controller instanceof AllExecutorsController) menu.getItem(2).setEnabled(false);
		*/
		
	}
	
	static Fragment getCurrentFragment(FragmentManager fragmentManager)
	{
		int count = fragmentManager.getBackStackEntryCount();
		if(BuildConfig.DEBUG) Log.d(TAG, "Activity.getCurrentFragment from backStack. entryCount: "+count);
		if(count == 0) return null;
		String tag = fragmentManager.getBackStackEntryAt(count-1).getName();
		Fragment fragment = fragmentManager.findFragmentByTag(tag);
		if(BuildConfig.DEBUG) Log.d(TAG, "found controller for tag "+tag+": "+fragment);
		return fragment;
	}
	
	static void goMainActivity(Activity activity, String parent)
	{
		Class activityClass = MainActivityPre.class;
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) activityClass = MainActivityLP.class;
		Intent intent = new Intent(activity, activityClass);
		if(parent == null) parent = LastBidsController.class.getName();
		intent.putExtra(CONTROLLER_CLASS_NAME, parent);
		activity.startActivity(intent);
		activity.finish();
	}
	
	static void goBaseActivity(Activity activity, String controllerClassName, Bundle args)
	{
		Class activityClass = BaseActivityPre.class;
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) activityClass = BaseActivityLP.class;
		Intent intent = new Intent(activity, activityClass);
		Bundle bundle = new Bundle();
		bundle.putString(CONTROLLER_CLASS_NAME, controllerClassName);
		if(args != null) bundle.putBundle(ARGS, args);
		intent.putExtras(bundle);
		activity.startActivity(intent);
		activity.finish();
	}
	
	default void processNavigationView(final FragmentActivity activity, NavigationView navigationView, DrawerLayout drawerLayout)
	{
		navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener()
		{
			@Override
			public boolean onNavigationItemSelected(MenuItem menuItem)
			{
				if(menuItem.isChecked()) menuItem.setChecked(false);
				else menuItem.setChecked(true);
				drawerLayout.closeDrawers();
				
				if(menuItem.getItemId() == R.id.exit)
				{
					User user = User.getUser(activity);
					if(BuildConfig.DEBUG) Log.d(TAG, "exit");
					if(user != null)
					{
						user.setLoggedIn(false);
						user.save(activity);
					}
					activity.finish();
					return true;
				}
				/*
				else if(menuItem.getItemId() == R.id.my_bids)
				{
					setTitle(R.string.menu_my_bids);
					return true;
				}
				*/
				else if(menuItem.getItemId() == R.id.last_bids)
				{
					setTitle(R.string.main_title);
					setFragment(activity, new LastBidsController(), true);
					return true;
				}
				else if(menuItem.getItemId() == R.id.bids_in_wait)
				{
					setTitle(R.string.bids_in_wait);
					LastBidsController controller = new LastBidsController();
					Bundle bundle = new Bundle();
					bundle.putInt(LastBidsController.TASK_FILTER, LastBidsController.FILTER_IN_WAIT);
					controller.setArguments(bundle);
					setFragment(activity, controller, true);
					return true;
				}
				else if(menuItem.getItemId() == R.id.bids_incomplete)
				{
					setTitle(R.string.bids_incomplete);
					LastBidsController controller = new LastBidsController();
					Bundle bundle = new Bundle();
					bundle.putInt(LastBidsController.TASK_FILTER, LastBidsController.FILTER_INCOMPLETE);
					controller.setArguments(bundle);
					setFragment(activity, controller, true);
					return true;
				}
				else if(menuItem.getItemId() == R.id.bids_in_work)
				{
					setTitle(R.string.bids_in_work);
					LastBidsController controller = new LastBidsController();
					Bundle bundle = new Bundle();
					bundle.putInt(LastBidsController.TASK_FILTER, LastBidsController.FILTER_IN_WORK);
					controller.setArguments(bundle);
					setFragment(activity, controller, true);
					return true;
				}
				else if(menuItem.getItemId() == R.id.all_executors)
				{
					setTitle(R.string.executors_title);
					setFragment(activity, new AllExecutorsController(), true);
					return true;
				}
				
				else if(true)
				{
					// fragment transaction
					if(BuildConfig.DEBUG) Log.d(TAG, "menu item: "+menuItem.getTitle());
					return true;
				}
				return false;
			}
		});
	}
	
	@LayoutRes
	int getLayout();
	void initControls();
	void showToolbar(boolean visible, String title);
	void setTitle(int titleId);
	void showProgressDialog(@StringRes int title, DialogInterface.OnCancelListener listener);
	void cancelProgressDialog();
	void showAlertDialog(@StringRes int title, String msg);
	
	public static final String CONTROLLER_CLASS_NAME = "controller";
	public static final String ARGS = "args";
}