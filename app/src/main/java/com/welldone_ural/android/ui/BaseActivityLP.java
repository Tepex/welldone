package com.welldone_ural.android.ui;

import android.annotation.TargetApi;

import android.app.AlertDialog;
import android.app.ProgressDialog;

import android.content.DialogInterface;
import android.content.Intent;

import android.os.Bundle;

import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.StringRes;
import android.support.v4.app.FragmentActivity;

import android.util.Log;

import android.view.MenuItem;
import android.view.View;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toolbar;

import com.welldone_ural.android.BuildConfig;
import com.welldone_ural.android.controller.BaseController;
import com.welldone_ural.android.controller.LastBidsController;
import com.welldone_ural.android.R;

import static com.welldone_ural.android.Utils.TAG;

@TargetApi(21)
public class BaseActivityLP extends FragmentActivity implements CompatibleActivity
{
	@CallSuper
	@Override
	protected void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		onCreate(this, bundle);
	}
	
	@CallSuper
	@Override
	public void onSaveInstanceState(Bundle bundle)
	{
		saveCurrentFragment(this, bundle);
		super.onSaveInstanceState(bundle);
	}
	
	@CallSuper
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		return isUpButtonPressed(this, item);
	}
	
	@CallSuper
	@Override
	public void onBackPressed()
	{
		if(!isExit(this)) super.onBackPressed();
	}
	
	@CallSuper
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		BaseController controller = (BaseController)CompatibleActivity.getCurrentFragment(getSupportFragmentManager());
		if(controller != null) controller.onActivityResult(requestCode, resultCode, data);
	}
	
	@Override
	public @LayoutRes int getLayout()
	{
		return R.layout.base_layout;
	}
	
	@Override
	public void initControls()
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "init controls");
		
		toolbar = (Toolbar)findViewById(R.id.toolbar);
		setActionBar(toolbar);
	}
	
	@Override
	public void showToolbar(boolean visible, String title)
	{
		if(visible)
		{
			getActionBar().show();
			getActionBar().setDisplayHomeAsUpEnabled(true);
			getActionBar().setTitle(title);
		}
		else getActionBar().hide();
	}
	
	@Override
	public void setTitle(int titleId)
	{
		getActionBar().setTitle(titleId);
	}
	
	@Override
	public void showProgressDialog(@StringRes int title, DialogInterface.OnCancelListener listener)
	{
		cancelProgressDialog();
		progressDialog = ProgressDialog.show(this, getString(title), getString(R.string.progress_wait), true, true, listener);
	}
	
	@Override
	public void cancelProgressDialog()
	{
		if(progressDialog != null)
		{
			progressDialog.dismiss();
			progressDialog = null;
		}
	}
	
	@Override
	public void showAlertDialog(@StringRes int title, String msg)
	{
		cancelProgressDialog();
		alertDialog = new AlertDialog.Builder(this)
			.setMessage(msg)
			.setTitle(title)
			.setPositiveButton(R.string.ok, (DialogInterface dialog, int id)->alertDialog.dismiss())
			.create();
		alertDialog.show();
	}
	
	protected Toolbar toolbar;
	protected ProgressDialog progressDialog;
	protected AlertDialog alertDialog;
}
