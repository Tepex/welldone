package com.welldone_ural.android.ui;

import android.content.Context;
import android.content.res.Configuration;

import android.os.Bundle;

import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.StringRes;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;

import android.util.Log;

import android.view.MenuItem;
import android.view.View;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.welldone_ural.android.BuildConfig;
import com.welldone_ural.android.controller.AllExecutorsController;
import com.welldone_ural.android.controller.LastBidsController;
import com.welldone_ural.android.model.User;
import com.welldone_ural.android.model.VKUser;
import com.welldone_ural.android.R;
import com.welldone_ural.android.Utils;

import static com.welldone_ural.android.Utils.TAG;

public class MainActivityPre extends BaseActivityPre
{
	@CallSuper
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		
		user = User.getUser(this);
		String userName;
		int userBalance = 0;
		String avatar = null;
		if(user != null && user.isLoggedIn())
		{
			userName = user.getName();
			userBalance = user.getBalance();
			avatar = user.getAvatar();
		}
		else
		{
			VKUser vkUser = VKUser.getUser(this);
			if(vkUser == null)
			{
				showAlertDialog(R.string.err_title, getString(R.string.app_error, Utils.ERR_VK_NULL));
				if(BuildConfig.DEBUG) Log.e(TAG, "VK user is null!");
				finish();
				return;
			}
			userName = vkUser.getName();
			avatar = vkUser.getAvatar();
		}
		
		TextView nameView = (TextView)findViewById(R.id.user_name);
		nameView.setText(userName);
		TextView balanceView = (TextView)findViewById(R.id.user_balance);
		balanceView.setText(getString(R.string.balance, userBalance));
		if(avatar != null) Utils.displayAvatar(this, (ImageView)findViewById(R.id.avatar), avatar, null); 
		
		navigationView = (NavigationView)findViewById(R.id.navigation_view);
		
		// Initializing Drawer Layout and ActionBarToggle
		drawerLayout = (DrawerLayout)findViewById(R.id.drawer);
		actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
		{
			@Override
			public void onDrawerClosed(View drawerView)
			{
				super.onDrawerClosed(drawerView);
			}
			
			@Override
			public void onDrawerOpened(View drawerView)
			{
				super.onDrawerOpened(drawerView);
			}
		};
		drawerLayout.setDrawerListener(actionBarDrawerToggle);
		actionBarDrawerToggle.syncState();
		processNavigationView(this, navigationView, drawerLayout);

		/*
		Spinner spinner = (Spinner)findViewById(R.id.bid_filter);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.task_filter, R.layout.custom_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(adapter);
		spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
		{
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int pos, long id)
			{
				if(BuildConfig.DEBUG) Log.d(TAG, "filter pos="+pos+" id="+id);
				LastBidsController controller = new LastBidsController();
				Bundle bundle = new Bundle();
				bundle.putInt(LastBidsController.TASK_FILTER, pos);
				controller.setArguments(bundle);
				CompatibleActivity.setFragment(MainActivityPre.this, controller, false);
			}
			
			@Override
			public void onNothingSelected(AdapterView<?> parent) {}
		});
		*/
	}
	
	@Override
	protected void onPostCreate(Bundle savedInstanceState)
	{
		super.onPostCreate(savedInstanceState);
		actionBarDrawerToggle.syncState();
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig)
	{
		super.onConfigurationChanged(newConfig);
		actionBarDrawerToggle.onConfigurationChanged(newConfig);
	}	
	
	@Override
	public @LayoutRes int getLayout()
	{
		return R.layout.main_layout;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if(actionBarDrawerToggle.onOptionsItemSelected(item)) return true;
		return super.onOptionsItemSelected(item);
	}	

	/*
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}
 
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
 
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
 
        return super.onOptionsItemSelected(item);
    }
    */
    
    public void goProfile(View view)
    {
    	if(BuildConfig.DEBUG) Log.d(TAG, "goProfile");
		drawerLayout.closeDrawers();
    }
    
    protected User user;
	protected NavigationView navigationView;
	protected DrawerLayout drawerLayout;
	protected ActionBarDrawerToggle actionBarDrawerToggle;
}