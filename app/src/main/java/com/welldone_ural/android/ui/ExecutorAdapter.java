package com.welldone_ural.android.ui;

import android.app.Activity;

import android.content.Context;

import android.graphics.Color;

import android.support.annotation.StringRes;

import android.util.Log;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import com.welldone_ural.android.BuildConfig;
import com.welldone_ural.android.model.Entity;
import com.welldone_ural.android.model.User;
import com.welldone_ural.android.R;
import com.welldone_ural.android.Utils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import java.util.Locale;

import static com.welldone_ural.android.Utils.TAG;

public class ExecutorAdapter extends ArrayAdapter<User>
{
	public ExecutorAdapter(Context context, User[] executors)
	{
		super(context, R.layout.executor_row, executors);
		this.context = context;
		inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public long getItemId(int pos)
	{
		return getItem(pos).getId();
	}
	
	@Override
	public View getView(int pos, View convertView, ViewGroup parent)
	{
		ViewHolder holder;
		if(convertView == null)
		{
			convertView = inflater.inflate(R.layout.executor_row, parent, false);
			holder = new ViewHolder();
			
			holder.nameView = (TextView)convertView.findViewById(R.id.name);
			holder.aboutView = (TextView)convertView.findViewById(R.id.about);
			holder.ratesView = (TextView)convertView.findViewById(R.id.rates);
			holder.avatarView = (ImageView)convertView.findViewById(R.id.avatar);
			convertView.setTag(holder);
		}
		else holder = (ViewHolder)convertView.getTag();
		
		User item = getItem(pos);
		holder.nameView.setText(item.getName());
		holder.aboutView.setText(item.getAbout());
		holder.ratesView.setText(" "+decimalFormat.format(item.getRating())+" ("+item.getCount()+")");
		
		Picasso picasso = Picasso.with(context);
		RequestCreator rc;
		if(item.getAvatarUrl() == null) rc = picasso.load(R.drawable.ic_avatar_none);
		else rc = picasso.load(item.getAvatarUrl());
		rc.resize(60, 60).centerCrop().into(holder.avatarView);
		return convertView;
	}
	
	private Context context;
	private LayoutInflater inflater;
	private DecimalFormat decimalFormat = new DecimalFormat("0.0", new DecimalFormatSymbols(Locale.US));
	
	static class ViewHolder
	{
		TextView nameView;
		TextView aboutView;
		TextView ratesView;
		ImageView avatarView;
	}
}
