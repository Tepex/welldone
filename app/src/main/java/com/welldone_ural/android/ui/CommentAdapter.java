package com.welldone_ural.android.ui;

import android.app.Activity;

import android.content.Context;

import android.graphics.Color;

import android.support.annotation.StringRes;

import android.util.Log;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import com.welldone_ural.android.BuildConfig;
import com.welldone_ural.android.model.Comment;
import com.welldone_ural.android.R;

import java.util.ArrayList;
import java.util.List;

import static com.welldone_ural.android.Utils.TAG;

public class CommentAdapter extends ArrayAdapter<Comment>
{
	public CommentAdapter(Context context, Comment[] comments)
	{
		super(context, R.layout.comment_row, comments);
		if(BuildConfig.DEBUG) Log.d(TAG, "CommentAdapter.init "+comments.length);
		
		this.context = context;
		inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		bottomGap = new View(context);
		bottomGap.setLayoutParams(new AbsListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 60));
		bottomGap.setBackgroundColor(Color.WHITE);
	}
	
	@Override
	public long getItemId(int pos)
	{
		return getItem(pos).getId();
	}
	
	@Override
	public View getView(int pos, View convertView, ViewGroup parent)
	{
		/* грязный хак. Влепляем в конец списка заглушку цфета фона и высотой такой, чтобы была чуть выше формы отправки сообщения
		иначе оригинальное сообщение в конце списка может быть частично перекрыто этой формой */
		if(pos == (getCount()-1)) return bottomGap;
		
		ViewHolder holder;
		if(convertView == null || convertView == bottomGap)
		{
			convertView = inflater.inflate(R.layout.comment_row, parent, false);
			holder = new ViewHolder();
			holder.authorNameView = (TextView)convertView.findViewById(R.id.user_name);
			holder.dateView = (TextView)convertView.findViewById(R.id.date);
			holder.messageView = (TextView)convertView.findViewById(R.id.comment);
			holder.imageView = (ImageView)convertView.findViewById(R.id.avatar);
			convertView.setTag(holder);
		}
		else holder = (ViewHolder)convertView.getTag();
		
		Comment item = getItem(pos);
		holder.authorNameView.setText(item.getUserName());
		holder.dateView.setText(item.getDate());
		holder.messageView.setText(item.getMessage());
		
		Picasso picasso = Picasso.with(context);
		RequestCreator rc;
		if(item.getAvatarUrl() == null) rc = picasso.load(R.drawable.ic_avatar_none);
		else rc = picasso.load(item.getAvatarUrl());
		rc.resize(60, 60).centerCrop().into(holder.imageView);
		
		return convertView;
	}
	
	private Context context;
	private LayoutInflater inflater;
	private View bottomGap;
	
	static class ViewHolder
	{
		TextView authorNameView;
		TextView dateView;
		TextView messageView;
		ImageView imageView;
	}
}
