package com.welldone_ural.android.controller;

import android.content.DialogInterface;

import android.os.Build;
import android.os.Bundle;

import android.support.annotation.StringRes;
import android.support.v4.widget.SwipeRefreshLayout;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;

import android.util.Log;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.octo.android.robospice.persistence.DurationInMillis;

import com.welldone_ural.android.BuildConfig;
import com.welldone_ural.android.model.Bid;
import com.welldone_ural.android.model.Comment;
import com.welldone_ural.android.model.Result;
import com.welldone_ural.android.model.User;
import com.welldone_ural.android.network.CommentRequest;
import com.welldone_ural.android.network.NewCommentRequest;
import com.welldone_ural.android.R;
import com.welldone_ural.android.ui.CommentAdapter;
import com.welldone_ural.android.ui.CompatibleActivity;

import static com.welldone_ural.android.Utils.TAG;

public class CommentsController extends BaseController
{
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		return super.onCreateView(inflater, container, savedInstanceState, R.layout.comments_fragment);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);
		if(savedInstanceState == null) savedInstanceState = getArguments();
		bid = (Bid)savedInstanceState.getParcelable(BidController.ARG_BID);
		if(BuildConfig.DEBUG) Log.d(TAG, "Comments for bid: "+bid);
		if(bid != null) title = bid.getName();
		
		listView = (ListView)view.findViewById(R.id.chat);
		emptyView = view.findViewById(R.id.empty_view);
		
		swipeLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipe_container);
		swipeLayout.setColorSchemeResources(R.color.sp1, R.color.sp2, R.color.sp3, R.color.sp4, R.color.sp5, R.color.sp6, R.color.sp7);
		swipeLayout.setOnRefreshListener(()->loadList());
		swipeLayout.post(()->loadList());
		
		final ImageButton send = (ImageButton)view.findViewById(R.id.bt_send);
		send.setOnClickListener((View v)->createNewComment());
		send.setEnabled(false);
		
		textField = (EditText)view.findViewById(R.id.input_text);
		textField.addTextChangedListener(new TextWatcher()
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
				send.setEnabled(!TextUtils.isEmpty(s));
			}
		
			@Override
			public void afterTextChanged(Editable editable) {}
		
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
		});
	}
	
	private void createNewComment()
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "new comment "+textField.getText());
		User user = User.getUser(getContext());
		String msg = textField.getText().toString();
		if(TextUtils.isEmpty(msg)) return;
		request(new NewCommentRequest(user.getEmail(), user.getPwd(), bid.getId(), msg), Comment.CACHE_KEY, DurationInMillis.ALWAYS_EXPIRED, true, R.string.sending_msg_title);
	}
	
	@Override
	public void onRequestSuccess(Object result)
	{
		super.onRequestSuccess(result);
		swipeLayout.setRefreshing(false);
		if(BuildConfig.DEBUG) Log.d(TAG, "qqq "+result);
		
		if(result instanceof Comment[])
		{
			Comment[] c = (Comment[])result;
			if(c.length > 0)
			{
				Comment[] c1 = new Comment[c.length+1];
				System.arraycopy(c, 0, c1, 0, c.length);
				c1[c1.length-1] = new Comment();
				c = c1;
			}
			listView.setAdapter(new CommentAdapter(getActivity(), c));
			listView.setEmptyView(emptyView);
		}
		else
		{
			((CompatibleActivity)getActivity()).showAlertDialog(R.string.msg_sent_title, getString(R.string.msg_sent));
			if(BuildConfig.DEBUG) Log.d(TAG, "qqqqqqqqqq");
			
		}
	}
	
	private void loadList()
	{
		swipeLayout.setRefreshing(true);
		if(BuildConfig.DEBUG) Log.d(TAG, "load comments.");
		
		request(new CommentRequest(bid.getId()), Bid.CACHE_KEY, DurationInMillis.ALWAYS_EXPIRED, false, 0);
	}
	
	
	@Override
	public void onSaveInstanceState(Bundle bundle)
	{
		bundle.putParcelable(BidController.ARG_BID, bid);
	}
	
	@Override
	public String getTitle()
	{
		return title;
	}

	private String title = "Comments";
	private Bid bid;
	private SwipeRefreshLayout swipeLayout;
	private EditText textField;
	private ListView listView;
	private View emptyView;
}