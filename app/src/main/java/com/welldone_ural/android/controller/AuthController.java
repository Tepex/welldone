package com.welldone_ural.android.controller;

import android.content.Context;

import android.os.Bundle;

import android.support.annotation.StringRes;
import android.support.design.widget.TextInputLayout;

import android.util.Log;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.EditText;

import com.octo.android.robospice.persistence.DurationInMillis;

import com.welldone_ural.android.BuildConfig;
import com.welldone_ural.android.R;

import com.welldone_ural.android.model.User;
import com.welldone_ural.android.network.AuthRequest;
import com.welldone_ural.android.ui.CompatibleActivity;

import static com.welldone_ural.android.Utils.TAG;

public class AuthController extends BaseController<User>
{
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		return super.onCreateView(inflater, container, savedInstanceState, R.layout.auth_fragment);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);
		title = getString(R.string.login_title);
		emailEdit = (EditText)view.findViewById(R.id.et_email);
		emailTil = (TextInputLayout)view.findViewById(R.id.til_email);
		
		pwdEdit = (EditText)view.findViewById(R.id.et_pwd);
		pwdTil = (TextInputLayout)view.findViewById(R.id.til_pwd);
		
		enterButton = (Button)view.findViewById(R.id.bt_login);
		enterButton.setEnabled(false);
		enterButton.setOnClickListener((View v)->
		{
			String email = emailEdit.getText().toString().trim();
			String pwd = pwdEdit.getText().toString().trim();
			if(BuildConfig.DEBUG) Log.d(TAG, "enter email="+email+" pwd="+pwd);
			request(new AuthRequest(email, pwd), User.CACHE_KEY, DurationInMillis.ALWAYS_EXPIRED, true, R.string.login_title);
		});
		
		FormHolder holder = new FormHolder(enterButton);
		FormHolder.ItemView[] item = new FormHolder.ItemView[1];
		item[0] = new FormHolder.ItemView(emailEdit, emailTil, getString(R.string.reg_email_err));
		holder.add(item);
		item[0] = new FormHolder.ItemView(pwdEdit, pwdTil, getString(R.string.reg_pwd_empty));
		holder.add(item);
		
		forgotButton = (Button)view.findViewById(R.id.bt_forgot);
		forgotButton.setOnClickListener((View v)->go(new ForgotController(), true));
	}
	
	@Override
	public void onRequestSuccess(User user)
	{
		super.onRequestSuccess(user);
		if(BuildConfig.DEBUG) Log.d(TAG, "result "+user);
		if(user.getId() > 0)
		{
			user.setPwd(pwdEdit.getText().toString().trim());
			user.setLoggedIn(true);
			user.save(getActivity());
			CompatibleActivity.goMainActivity(getActivity(), null);
			return;
		}
		else
		{
			spiceManager.removeDataFromCache(User.class);
			((CompatibleActivity)getActivity()).showAlertDialog(R.string.err_auth_title, getString(R.string.err_auth_msg));
			forgotButton.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public String getTitle()
	{
		return title;
	}
	
	private EditText emailEdit;
	private EditText pwdEdit;
	private TextInputLayout emailTil;
	private TextInputLayout pwdTil;
	private Button enterButton;
	private Button forgotButton;
	private String title;
}