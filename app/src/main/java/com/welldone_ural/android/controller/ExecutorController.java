package com.welldone_ural.android.controller;

import android.app.Activity;
import android.app.Fragment;

import android.content.Context;
import android.content.Intent;

import android.os.Build;
import android.os.Bundle;

import android.support.annotation.StringRes;

import android.util.Log;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import com.welldone_ural.android.BuildConfig;
import com.welldone_ural.android.model.Result;
import com.welldone_ural.android.model.User;
import com.welldone_ural.android.R;
import com.welldone_ural.android.ui.CompatibleActivity;

import static com.welldone_ural.android.Utils.TAG;

public class ExecutorController extends BaseController
{
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		return super.onCreateView(inflater, container, savedInstanceState, R.layout.executor_fragment);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);
		if(savedInstanceState == null) savedInstanceState = getArguments();
		user = (User)savedInstanceState.getParcelable(ARG_USER);
		if(user != null) title = user.getName();
		
		ImageView avatar = (ImageView)view.findViewById(R.id.avatar);
		Picasso picasso = Picasso.with(getActivity());
		RequestCreator rc;
		if(user.getAvatarUrl() == null) rc = picasso.load(R.drawable.ic_avatar_none);
		else rc = picasso.load(user.getAvatarUrl());
		
		/*
		int gapInPixels = ((int)getResources().getDimension(R.dimen.rootGapX)) * 2;
		
		DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
		float dpHeight = displayMetrics.heightPixels / displayMetrics.density;
		float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
		float maxWidth
		*/
		rc.resize(100, 100).centerCrop().into(avatar);
		
		((TextView)view.findViewById(R.id.about)).setText(user.getAbout());
	}
	
	@Override
	public void onSaveInstanceState(Bundle bundle)
	{
		bundle.putParcelable(ARG_USER, user);
	}
	
	@Override
	public String getTitle()
	{
		return title;
	}
	
	private String title;
	private User user;
	private String parent;
	
	public static final String ARG_USER = "user";
}