package com.welldone_ural.android.controller;

import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;

import android.support.annotation.StringRes;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.SwipeRefreshLayout;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;

import android.util.Log;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.octo.android.robospice.persistence.DurationInMillis;

import com.welldone_ural.android.BuildConfig;
import com.welldone_ural.android.model.Result;
import com.welldone_ural.android.model.User;
import com.welldone_ural.android.network.ExecutorsRequest;
import com.welldone_ural.android.R;
import com.welldone_ural.android.ui.CompatibleActivity;
import com.welldone_ural.android.ui.ExecutorAdapter;

import static com.welldone_ural.android.Utils.TAG;

public class AllExecutorsController extends BaseController<User[]>
{
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		return super.onCreateView(inflater, container, savedInstanceState, R.layout.executors_fragment);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);
		title = getString(R.string.executors_title);
		listView = (ListView)view.findViewById(R.id.executors_list);
		emptyView = view.findViewById(R.id.empty_view);
		listView.setOnItemClickListener((AdapterView<?> parent, View v, int pos, long id)->
		{
			if(BuildConfig.DEBUG) Log.d(TAG, "list item click "+pos+" "+parent.getItemAtPosition(pos));
			
			Bundle args = new Bundle();
			args.putParcelable(ExecutorController.ARG_USER, (Parcelable)parent.getItemAtPosition(pos));
			args.putString(ARG_PARENT, AllExecutorsController.class.getName());
			CompatibleActivity.goBaseActivity(getActivity(), ExecutorController.class.getName(), args);
		});
		
		swipeLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipe_container);
		swipeLayout.setColorSchemeResources(R.color.sp1, R.color.sp2, R.color.sp3, R.color.sp4, R.color.sp5, R.color.sp6, R.color.sp7);
		swipeLayout.setOnRefreshListener(()->loadList());
		swipeLayout.post(()->loadList());
	}
	
	public void onRequestSuccess(User[] executors)
	{
		super.onRequestSuccess(executors);
		swipeLayout.setRefreshing(false);
		listView.setAdapter(new ExecutorAdapter(getActivity(), executors));
		listView.setEmptyView(emptyView);
	}
	
	private void loadList()
	{
		swipeLayout.setRefreshing(true);
		request(new ExecutorsRequest(), User.CACHE_KEY_EXECUTOR, DurationInMillis.ALWAYS_EXPIRED, false, 0);
	}

	@Override
	public String getTitle()
	{
		return title;
	}
	
	private SwipeRefreshLayout swipeLayout;
	private ListView listView;
	private View emptyView;
	private String title;
}