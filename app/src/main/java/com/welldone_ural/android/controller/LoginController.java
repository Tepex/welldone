package com.welldone_ural.android.controller;

import android.content.Context;

import android.os.Bundle;
import android.util.Log;
import android.support.annotation.StringRes;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.TextView;

import com.octo.android.robospice.persistence.DurationInMillis;

import com.welldone_ural.android.BuildConfig;
import com.welldone_ural.android.R;

import com.welldone_ural.android.model.User;
import com.welldone_ural.android.network.AuthRequest;
import com.welldone_ural.android.ui.CompatibleActivity;

import static com.welldone_ural.android.Utils.TAG;

public class LoginController extends BaseController<User>
{
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		return super.onCreateView(inflater, container, savedInstanceState, R.layout.login_fragment);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);
		title = getString(R.string.login_title);
		user = User.getUser(getActivity());
		TextView textView = (TextView)view.findViewById(R.id.user_name);
		textView.setText(getString(R.string.login_as, user.getName()));
		
		((Button)view.findViewById(R.id.bt_login)).setOnClickListener((View v)->
		{
			if(BuildConfig.DEBUG) Log.d(TAG, "enter email="+user.getEmail()+" pwd="+user.getPwd());
			request(new AuthRequest(user.getEmail(), user.getPwd()), User.CACHE_KEY, DurationInMillis.ALWAYS_EXPIRED, true, R.string.login_title);
		});
		((Button)view.findViewById(R.id.bt_change_login)).setOnClickListener((View v)->
		{
			user.remove(getActivity());
			go(new UserNewController(), true);
		});
	}

	@Override
	public void onRequestSuccess(User newUser)
	{
		super.onRequestSuccess(user);
		if(BuildConfig.DEBUG) Log.d(TAG, "result "+newUser);
		if(newUser.getId() > 0)
		{
			user.update(newUser);
			user.save(getActivity()); 
			CompatibleActivity.goMainActivity(getActivity(), null);
			return;
		}
		else
		{
			user.remove(getActivity());
			spiceManager.removeDataFromCache(User.class);
			((CompatibleActivity)getActivity()).showAlertDialog(R.string.err_auth_title, getString(R.string.err_auth_msg));
			go(new UserNewController(), true);
		}
	}

	@Override
	public String getTitle()
	{
		return title;
	}
	
	@Override
	public boolean hasToolbar()
	{
		return false;
	}
	
	private User user;
	private String title;
}