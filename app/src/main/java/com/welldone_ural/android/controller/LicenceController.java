package com.welldone_ural.android.controller;

import android.content.DialogInterface;

import android.os.Build;
import android.os.Bundle;

import android.support.annotation.StringRes;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.webkit.WebView;
import android.webkit.WebViewClient;

import android.widget.Button;

import com.welldone_ural.android.R;
import com.welldone_ural.android.ui.CompatibleActivity;

public class LicenceController extends BaseController
{
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		return super.onCreateView(inflater, container, savedInstanceState, R.layout.licence_fragment);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);
		title = getString(R.string.licence_title);
		final CompatibleActivity activity = (CompatibleActivity)getActivity();
		
		final WebView webView = (WebView)view.findViewById(R.id.web_view);
		webView.setWebViewClient(new WebViewClient()
		{
			@Override
			public void onPageFinished(WebView view, String url)
			{
				activity.cancelProgressDialog();
				continueButton.setEnabled(true);
				continueButton.setClickable(true);
			}
		});
		webView.loadUrl(getString(R.string.licence_url));
		activity.showProgressDialog(R.string.loading_title, (DialogInterface di)->webView.stopLoading());
		
		continueButton = (Button)view.findViewById(R.id.bt_continue);
		continueButton.setEnabled(false);
		continueButton.setClickable(false);
		continueButton.setOnClickListener((View v)->go(new RegistrationController(), true));
	}
	
	@Override
	public String getTitle()
	{
		return title;
	}
	
	private Button continueButton;
	private String title;
}