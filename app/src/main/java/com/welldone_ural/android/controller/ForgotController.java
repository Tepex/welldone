package com.welldone_ural.android.controller;

import android.content.Context;

import android.os.Bundle;

import android.support.annotation.StringRes;
import android.support.design.widget.TextInputLayout;

import android.util.Log;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.EditText;

import com.welldone_ural.android.BuildConfig;
import com.welldone_ural.android.R;

import static com.welldone_ural.android.Utils.TAG;

public class ForgotController extends BaseController
{
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		return super.onCreateView(inflater, container, savedInstanceState, R.layout.forgot_fragment);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);
		title = getString(R.string.forgot_title);
		emailEdit = (EditText)view.findViewById(R.id.et_email);
		emailTil = (TextInputLayout)view.findViewById(R.id.til_email);
		
		sendButton = (Button)view.findViewById(R.id.bt_send);
		sendButton.setEnabled(false);
		sendButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if(BuildConfig.DEBUG) Log.d(TAG, "send password");
			}
		});
		
		FormHolder holder = new FormHolder(sendButton);
		FormHolder.ItemView[] item = {new FormHolder.ItemView(emailEdit, emailTil, getString(R.string.reg_email_err))};
		holder.add(item);
	}
	
	@Override
	public String getTitle()
	{
		return title;
	}
	
	private EditText emailEdit;
	private TextInputLayout emailTil;
	private Button sendButton;
	private String title;
}
