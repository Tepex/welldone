package com.welldone_ural.android.controller;

import android.app.Activity;

import android.content.Context;
import android.content.Intent;

import android.net.Uri;

import android.os.Bundle;

import android.provider.MediaStore;

import android.support.annotation.StringRes;
import android.support.design.widget.TextInputLayout;

import android.util.Log;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.octo.android.robospice.persistence.DurationInMillis;

import com.welldone_ural.android.BuildConfig;
import com.welldone_ural.android.R;

import com.welldone_ural.android.model.User;
import com.welldone_ural.android.network.RegRequest;
import com.welldone_ural.android.ui.CompatibleActivity;

import static com.welldone_ural.android.Utils.TAG;

public class RegistrationController extends BaseController<User>
{
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		return super.onCreateView(inflater, container, savedInstanceState, R.layout.registration_fragment);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);
		title = getString(R.string.reg_title);
		/*
		Button makeFotoButton = (Button)view.findViewById(R.id.bt_make_foto);
		makeFotoButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				avatarUri = Uri.fromFile(AVATAR_FILE);
				intent.putExtra(MediaStore.EXTRA_OUTPUT, avatarUri);
				RegistrationFragment.this.getActivity().startActivityForResult(intent, GET_PHOTO);
			}
		});

		Button fromGalleryButton = (Button)view.findViewById(R.id.bt_from_gallery);
		fromGalleryButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Intent intent = new Intent(Intent.ACTION_PICK);
				intent.setType("image/*");
				intent.setAction(Intent.ACTION_GET_CONTENT);
				RegistrationFragment.this.getActivity().startActivityForResult(
				Intent.createChooser(intent, RegistrationFragment.this.getString(R.string.chooser_title)), FROM_FILE);
			}
		});
		
		
		avatarView = (ImageView)view.findViewById(R.id.avatar);
		*/
		
		nameEdit = (EditText)view.findViewById(R.id.et_name);
		nameTil = (TextInputLayout)view.findViewById(R.id.til_name);
		
		
		
		
		
		emailEdit = (EditText)view.findViewById(R.id.et_email);
		emailTil = (TextInputLayout)view.findViewById(R.id.til_email);
		
		phoneEdit = (EditText)view.findViewById(R.id.et_phone);
		phoneTil = (TextInputLayout)view.findViewById(R.id.til_phone);
		
		pwdEdit = (EditText)view.findViewById(R.id.et_pwd);
		pwdTil = (TextInputLayout)view.findViewById(R.id.til_pwd);
		
		pwdRepeatEdit = (EditText)view.findViewById(R.id.et_pwd_repeat);
		pwdRepeatTil = (TextInputLayout)view.findViewById(R.id.til_pwd_repeat);

		processButton = (Button)view.findViewById(R.id.bt_process);
		processButton.setEnabled(false);
		processButton.setOnClickListener((View v)->request(
			new RegRequest(
				nameEdit.getText().toString().trim(),
				"surname",
				emailEdit.getText().toString().trim(),
				phoneEdit.getText().toString().trim(),
				pwdEdit.getText().toString().trim()),
			User.CACHE_KEY,
			DurationInMillis.ALWAYS_EXPIRED,
			true,
			R.string.reg_title));
		
		FormHolder holder = new FormHolder(processButton);
		FormHolder.ItemView[] item = new FormHolder.ItemView[1];
		item[0] = new FormHolder.ItemView(nameEdit, nameTil, getString(R.string.reg_login_err));
		holder.add(item);
		
		item[0] = new FormHolder.ItemView(emailEdit, emailTil, getString(R.string.reg_email_err));
		holder.add(item);
		
		item[0] = new FormHolder.ItemView(phoneEdit, phoneTil, getString(R.string.reg_phone_err));
		holder.add(item);
		
		item = new FormHolder.ItemView[2];
		item[0] = new FormHolder.ItemView(pwdEdit, pwdTil, getString(R.string.reg_pwd_err));
		item[1] = new FormHolder.ItemView(pwdRepeatEdit, pwdRepeatTil, getString(R.string.reg_pwd_err));
		holder.add(item);
	}
	
	@Override
	public void onRequestSuccess(User user)
	{
		super.onRequestSuccess(user);
		if(BuildConfig.DEBUG) Log.d(TAG, "result "+user);
		if(user.getId() > 0)
		{
			user.setPwd(pwdEdit.getText().toString().trim());
			user.setLoggedIn(true);
			user.save(getActivity());
			CompatibleActivity.goMainActivity(getActivity(), null);
			return;
		}
		else
		{
			spiceManager.removeDataFromCache(User.class);
			@StringRes int errMsg = R.string.err_unknown;
			if(user.getId() == User.ERR_EMAIL) errMsg = R.string.err_email_format;
			else if(user.getId() == User.ERR_EMAIL_ALLREADY_EXISTS) errMsg = R.string.err_email_exists;
			else if(user.getId() == User.ERR_PHONE_ALLREADY_EXISTS) errMsg = R.string.err_phone_exists;
			((CompatibleActivity)getActivity()).showAlertDialog(R.string.reg_title, getString(errMsg));
		}
	}
	
	@Override
	public String getTitle()
	{
		return title;
	}

	private EditText nameEdit;
	private EditText emailEdit;
	private EditText phoneEdit;
	private EditText pwdEdit;
	private EditText pwdRepeatEdit;
	
	private TextInputLayout nameTil;
	private TextInputLayout emailTil;
	private TextInputLayout phoneTil;
	private TextInputLayout pwdTil;
	private TextInputLayout pwdRepeatTil;
	
	private ImageView avatarView;
	private Button processButton;
	private String title;
}