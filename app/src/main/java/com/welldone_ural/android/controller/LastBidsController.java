package com.welldone_ural.android.controller;

import android.content.Context;

import android.os.Bundle;
import android.os.Parcelable;

import android.support.annotation.StringRes;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.SwipeRefreshLayout;

import android.util.Log;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import com.welldone_ural.android.BuildConfig;
import com.welldone_ural.android.model.Bid;
import com.welldone_ural.android.model.User;
import com.welldone_ural.android.network.BidRequest;
import com.welldone_ural.android.network.BidsIncompleteRequest;
import com.welldone_ural.android.network.BidsInWaitRequest;
import com.welldone_ural.android.network.BidsInWorkRequest;
import com.welldone_ural.android.network.RestApi;
import com.welldone_ural.android.R;
import com.welldone_ural.android.ui.BidAdapter;
import com.welldone_ural.android.ui.CompatibleActivity;

import static com.welldone_ural.android.Utils.TAG;

public class LastBidsController extends BaseController<Bid[]>
{
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		return super.onCreateView(inflater, container, savedInstanceState, R.layout.last_bids_fragment);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);
		title = getString(R.string.main_title);
		listView = (ListView)view.findViewById(R.id.bid_list);
		emptyView = view.findViewById(R.id.empty_view);
		listView.setOnItemClickListener((AdapterView<?> parent, View v, int pos, long id)->
		{
			if(BuildConfig.DEBUG) Log.d(TAG, "list item click "+pos+" "+parent.getItemAtPosition(pos));
			Bundle args = new Bundle();
			args.putParcelable(BidController.ARG_BID, (Parcelable)parent.getItemAtPosition(pos));
			args.putString(ARG_PARENT, LastBidsController.class.getName());
			CompatibleActivity.goBaseActivity(getActivity(), BidController.class.getName(), args);
		});
		
		swipeLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipe_container);
		swipeLayout.setColorSchemeResources(R.color.sp1, R.color.sp2, R.color.sp3, R.color.sp4, R.color.sp5, R.color.sp6, R.color.sp7);
		swipeLayout.setOnRefreshListener(()->loadList());
		swipeLayout.post(()->loadList());
		
		fab = (FloatingActionButton)view.findViewById(R.id.fab);
		fab.setOnClickListener((View v)->createNewBid());
	}
	
	private void createNewBid()
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "FAB");
		/*
		Bundle args = new Bundle();
		args.putString(ARG_PARENT, getClass().getName());
		CompatibleActivity.goBaseActivity(getActivity(), NewBidController.class.getName(), args);
		*/
	}
	
	@Override
	public void onRequestSuccess(Bid[] bids)
	{
		super.onRequestSuccess(bids);
		swipeLayout.setRefreshing(false);
		listView.setAdapter(new BidAdapter(getActivity(), bids));
		listView.setEmptyView(emptyView);
	}
	
	private void loadList()
	{
		swipeLayout.setRefreshing(true);
		int filter = FILTER_ALL;
		Bundle bundle = getArguments();
		if(bundle != null) filter = bundle.getInt(TASK_FILTER, FILTER_ALL);
		if(BuildConfig.DEBUG) Log.d(TAG, "load list. filter = "+filter);
		
		User user = User.getUser(getActivity());
		if(filter == FILTER_ALL) request(new BidRequest(), Bid.CACHE_KEY, CACHE_DURATION, false, 0);
		else if(filter == FILTER_IN_WAIT) request(new BidsInWaitRequest(user.getEmail(), user.getPwd()), Bid.CACHE_KEY_IN_WAIT, CACHE_DURATION, false, 0);
		else if(filter == FILTER_INCOMPLETE) request(new BidsIncompleteRequest(user.getEmail(), user.getPwd()), Bid.CACHE_KEY_INCOMPLETE, CACHE_DURATION, false, 0);
		else if(filter == FILTER_IN_WORK) request(new BidsInWorkRequest(user.getEmail(), user.getPwd()), Bid.CACHE_KEY_IN_WORK, CACHE_DURATION, false, 0);
	}

	@Override
	public String getTitle()
	{
		return title;
	}

	private String title;
	private ListView listView;
	private View emptyView;
	private SwipeRefreshLayout swipeLayout;
	private FloatingActionButton fab;
	public static final long CACHE_DURATION = 15 * DurationInMillis.ONE_MINUTE;
	//public static final long CACHE_DURATION = DurationInMillis.ALWAYS_EXPIRED;
	
	public static final String TASK_FILTER = "task_filter";
	public static final int FILTER_ALL = 0;
	public static final int FILTER_IN_WAIT = 1;
	public static final int FILTER_INCOMPLETE = 2;
	public static final int FILTER_IN_WORK = 3;
}