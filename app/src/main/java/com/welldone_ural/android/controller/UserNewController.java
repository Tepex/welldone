package com.welldone_ural.android.controller;

import android.app.Activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.os.Bundle;

import android.support.annotation.StringRes;
import android.support.design.widget.TextInputLayout;

import android.util.Log;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.octo.android.robospice.persistence.DurationInMillis;

import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;

import com.welldone_ural.android.BuildConfig;
import com.welldone_ural.android.model.VKUser;
import com.welldone_ural.android.model.VKUsersResponse;
import com.welldone_ural.android.model.User;

import com.welldone_ural.android.network.Rest;
import com.welldone_ural.android.network.AuthRequest;

import com.welldone_ural.android.R;
import com.welldone_ural.android.ui.CompatibleActivity;

import static com.welldone_ural.android.Utils.TAG;

public class UserNewController extends BaseController<User>
{
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		return super.onCreateView(inflater, container, savedInstanceState, R.layout.user_new_fragment);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);
		title = getString(R.string.login_title);
		emailEdit = (EditText)view.findViewById(R.id.et_email);
		emailTil = (TextInputLayout)view.findViewById(R.id.til_email);
		
		pwdEdit = (EditText)view.findViewById(R.id.et_pwd);
		pwdTil = (TextInputLayout)view.findViewById(R.id.til_pwd);
		
		enterButton = (Button)view.findViewById(R.id.bt_login);
		enterButton.setEnabled(false);
		enterButton.setOnClickListener((View v)->
		{
			String email = emailEdit.getText().toString().trim();
			String pwd = pwdEdit.getText().toString().trim();
			if(BuildConfig.DEBUG) Log.d(TAG, "enter email="+email+" pwd="+pwd);
			request(new AuthRequest(email, pwd), User.CACHE_KEY, DurationInMillis.ALWAYS_EXPIRED, true, R.string.login_title);
		});
		
		FormHolder holder = new FormHolder(enterButton);
		FormHolder.ItemView[] item = new FormHolder.ItemView[1];
		item[0] = new FormHolder.ItemView(emailEdit, emailTil, getString(R.string.reg_email_err));
		holder.add(item);
		item[0] = new FormHolder.ItemView(pwdEdit, pwdTil, getString(R.string.reg_pwd_empty));
		holder.add(item);
		
		
		/*
		ImageButton vkButton = (ImageButton)view.findViewById(R.id.bt_vk);
		vkButton.setOnClickListener((View v)->
		{
			((CompatibleActivity)getActivity()).showProgressDialog(R.string.login_title, (DialogInterface dialog)->{});
			VKSdk.login(getActivity(), VK_SCOPES);
		});
		*/
		
		Button registrationButton = (Button)view.findViewById(R.id.bt_reg);
		registrationButton.setOnClickListener((View v)->go(new LicenceController(), true));
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		if(BuildConfig.DEBUG) Log.d(TAG, "UserNewController.onActivityResult!");
		
		VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>()
		{
			@Override
			public void onResult(VKAccessToken res)
			{
				// User passed Authorization
				VKRequest request = VKApi.users().get(VKParameters.from(VKApiConst.FIELDS, "photo_max,contacts"));
				request.setPreferredLang("ru");
				request.useSystemLanguage = true;
				request.executeWithListener(new VKRequest.VKRequestListener()
				{
					@Override
					public void onComplete(final VKResponse response)
					{
						CompatibleActivity activity = (CompatibleActivity)getActivity();
						activity.cancelProgressDialog();
						VKUser vkUser = Rest.getGson().fromJson(response.json.toString(), VKUsersResponse.class).getUser();
						if(vkUser != null)
						{
							vkUser.save(getActivity());
							CompatibleActivity.goMainActivity(getActivity(), null);
						}
						else activity.showAlertDialog(R.string.err_auth_title, getString(R.string.err_auth_msg_vk));
					}
	
					@Override
					public void onError(final VKError error)
					{
						((CompatibleActivity)getActivity()).showAlertDialog(R.string.err_auth_title, error.toString());
					}
				});
			}
			
			@Override
			public void onError(VKError error)
			{
				((CompatibleActivity)getActivity()).showAlertDialog(R.string.err_auth_title, error.toString());
			}
		});
	}
	
	@Override
	public void onRequestSuccess(User user)
	{
		super.onRequestSuccess(user);
		if(BuildConfig.DEBUG) Log.d(TAG, "result "+user);
		
		/*
		if(user.getId() == 0)
		{
			User fake = new User(100, "Tepex", "123123", "tepex@mail.ru", "pwdpwd", null, "", 100, 1, true);
			user.update(fake);
		}
		
		*/
		
		if(user.getId() > 0)
		{
			user.setPwd(pwdEdit.getText().toString().trim());
			user.setLoggedIn(true);
			user.save(getActivity());
			if(BuildConfig.DEBUG) Log.d(TAG, "go MainActivity");
			
			CompatibleActivity.goMainActivity(getActivity(), null);
			return;
		}
		else
		{
			if(BuildConfig.DEBUG) Log.d(TAG, "user id is 0");
			
			spiceManager.removeDataFromCache(User.class);
			((CompatibleActivity)getActivity()).showAlertDialog(R.string.err_auth_title, getString(R.string.err_auth_msg));
			//forgotButton.setVisibility(View.VISIBLE);
		}
	}

	
	@Override
	public String getTitle()
	{
		return title;
	}
	
	@Override
	public boolean hasToolbar()
	{
		return false;
	}
	
	private EditText emailEdit;
	private EditText pwdEdit;
	private TextInputLayout emailTil;
	private TextInputLayout pwdTil;
	private Button enterButton;
	private Button forgotButton;
	private String title;
	
	private static final String[] VK_SCOPES = new String[]{VKScope.FRIENDS, VKScope.PHOTOS};
}
