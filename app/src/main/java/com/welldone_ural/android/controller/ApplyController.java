package com.welldone_ural.android.controller;

import android.os.Build;
import android.os.Bundle;

import android.support.annotation.StringRes;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;

import android.util.Log;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.octo.android.robospice.persistence.DurationInMillis;

import com.welldone_ural.android.BuildConfig;
import com.welldone_ural.android.R;

import com.welldone_ural.android.model.Bid;
import com.welldone_ural.android.model.Result;
import com.welldone_ural.android.model.User;

import com.welldone_ural.android.network.ApplyBidRequest;
import com.welldone_ural.android.ui.CompatibleActivity;

import static com.welldone_ural.android.Utils.TAG;

public class ApplyController extends BaseController<Result>
{
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		return super.onCreateView(inflater, container, savedInstanceState, R.layout.apply_fragment);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);
		if(savedInstanceState == null) savedInstanceState = getArguments();
		bid = (Bid)savedInstanceState.getParcelable(BidController.ARG_BID);
		if(BuildConfig.DEBUG) Log.d(TAG, "Comments for bid: "+bid);
		if(bid != null) title = bid.getName();
		
		priceEdit = (EditText)view.findViewById(R.id.et_price);
		confirmButton = (Button)view.findViewById(R.id.bt_confirm);
		confirmButton.setEnabled(false);
		
		priceEdit.setOnFocusChangeListener((View v, boolean hasFocus)->
		{
			if(!hasFocus) checkEdit();
		});
		priceEdit.addTextChangedListener(new TextWatcher()
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
				checkEdit();
			}
		
			@Override
			public void afterTextChanged(Editable editable) {}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
		});
		
		confirmButton.setOnClickListener((View v)->
		{
			User user = User.getUser(getContext());
			int price = 0;
			try
			{
				price = Integer.parseInt(priceEdit.getText().toString());
			}
			catch(NumberFormatException e)
			{
				if(BuildConfig.DEBUG) Log.e(TAG, "Price error! ", e);
				return;
			}
			request(new ApplyBidRequest(user.getId(), bid.getId(), price), Result.CACHE_KEY, DurationInMillis.ALWAYS_EXPIRED, true, R.string.check_bid_title);
			//onRequestSuccess(new Result("1", 3000));
		});
	}
	
	public void onRequestSuccess(Result result)
	{
		super.onRequestSuccess(result);
		spiceManager.removeDataFromCache(Bid[].class);
		bid.setPrice(result.getPrice());
		if(BuildConfig.DEBUG) Log.d(TAG, "ApplyController result: "+result+" bid: "+bid);
		Bundle args = new Bundle();
		args.putParcelable(BidController.ARG_BID, bid);
		args.putString(ARG_PARENT, LastBidsController.class.getName());
		BidController controller = new BidController();
		controller.setArguments(args);
		go(controller, false);
	}
	
	private void checkEdit()
	{
		confirmButton.setEnabled(!TextUtils.isEmpty(priceEdit.getText()));
	}
	
	@Override
	public void onSaveInstanceState(Bundle bundle)
	{
		bundle.putParcelable(BidController.ARG_BID, bid);
	}
	
	@Override
	public String getTitle()
	{
		return title;
	}

	private String title = "Apply";
	private Bid bid;
	
	private EditText priceEdit;
	private Button confirmButton;
}