package com.welldone_ural.android.controller;

import android.support.design.widget.TextInputLayout;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;

import android.view.View;

import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

class FormHolder extends ArrayList<FormHolder.ItemView[]>
{
	public FormHolder(Button actionButton)
	{
		super();
		this.actionButton = actionButton;
		this.actionButton.setEnabled(false);
	}
	
	@Override
	public boolean add(ItemView[] item)
	{
		EditTextWatcher watcher = new EditTextWatcher(this, item);
		for(ItemView view: item)
		{
			view.getEditText().setOnFocusChangeListener(watcher);
			view.getEditText().addTextChangedListener(watcher);
		}
		return super.add(item);
	}
	
	private void checkButton()
	{
		boolean enabled = true;
		for(ItemView[] item: this)
		{
			if(item.length == 1)
			{
				if(TextUtils.isEmpty(item[0].getText()))
				{
					enabled = false;
					break;
				}
			}
			else if(EditTextWatcher.isPwdError(item))
			{
				enabled = false;
				break;
			}
		}
		actionButton.setEnabled(enabled);
	}
	
	private Button actionButton;
	
	public static class ItemView
	{
		public ItemView(EditText editText, TextInputLayout til, String err)
		{
			this.editText = editText;
			this.til = til;
			this.err = err;
		}
		
		public EditText getEditText()
		{
			return editText;
		}
		
		public int getId()
		{
			return editText.getId();
		}
		
		public String getText()
		{
			return editText.getText().toString().trim();
		}
		
		public void setError(boolean check)
		{
			if(TextUtils.isEmpty(getText()) || !check) til.setError(err);
			else til.setError(null);
		}
		
		private EditText editText;
		private TextInputLayout til;
		private String err;
	}
	
	private static class EditTextWatcher implements TextWatcher, View.OnFocusChangeListener
	{
		public EditTextWatcher(FormHolder holder, ItemView[] item)
		{
			this.holder = holder;
			this.item = item;
		}
		
		@Override
		public void onFocusChange(View view, boolean hasFocus)
		{
			if(hasFocus) return;
			if(item.length == 1) item[0].setError(true);
			else
			{
				boolean err = isPwdError(item);
				item[0].setError(err);
				item[1].setError(err);
			}
			holder.checkButton();
		}
		
		private static boolean isPwdError(ItemView[] item)
		{
			String text1 = item[0].getText();
			String text2 = item[1].getText();
			return (TextUtils.isEmpty(text1) || !text1.equals(text2));
		}
		
		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count)
		{
			onFocusChange(null, false);
		}
		
		@Override
		public void afterTextChanged(Editable editable) {}
		
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
		
		private FormHolder holder;
		private ItemView[] item;
	}
}