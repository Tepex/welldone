package com.welldone_ural.android.controller;

import android.app.Activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.os.Build;
import android.os.Bundle;

import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import android.util.Log;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.octo.android.robospice.exception.NetworkException;
import com.octo.android.robospice.exception.RequestCancelledException;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.octo.android.robospice.SpiceManager;

import com.welldone_ural.android.BuildConfig;
import com.welldone_ural.android.network.RestApi;
import com.welldone_ural.android.network.SpiceService;
import com.welldone_ural.android.R;
import com.welldone_ural.android.ui.CompatibleActivity;

import java.lang.reflect.Constructor;

import static com.welldone_ural.android.Utils.TAG;

public abstract class BaseController<T> extends Fragment implements RequestListener<T>
{
	protected View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState, @LayoutRes int layoutRes)
	{
		return inflater.inflate(layoutRes, container, false);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		// Фрагмент не уничтожается при перезапуске Activity
		setRetainInstance(true);
	}
	
	@Override
	public void onStart()
	{
		super.onStart();
		spiceManager.start(getActivity());
	}
	
	@Override
	public void onStop()
	{
		if(spiceManager.isStarted()) spiceManager.shouldStop();
		super.onStop();
	}
	
	@Override
	public void onDestroy()
	{
		((CompatibleActivity)getActivity()).cancelProgressDialog();
		super.onDestroy();
	}
	
	/**
	 * Смена фрагмента в текущем Activity.
	 */
	public void go(Fragment fragment, boolean addToBackStack)
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "go fragment "+fragment.getClass().getName());
		FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
		fragmentTransaction.replace(R.id.content, fragment, fragment.getClass().getName());
		if(addToBackStack) fragmentTransaction.addToBackStack(fragment.getClass().getName());
		fragmentTransaction.commit();
	}
	
	@CallSuper
	protected void request(
		RetrofitSpiceRequest<T, RestApi> request,
		String cacheKey,
		long duration,
		boolean showProgressDialog,
		@StringRes int titleRes)
	{
		if(showProgressDialog)
		{
			((CompatibleActivity)getActivity()).showProgressDialog(titleRes, (DialogInterface dialog)->spiceManager.cancelAllRequests());
		}
		spiceManager.execute(request, cacheKey, duration, this);	
	}
	
	@CallSuper
	@Override
	public void onRequestSuccess(T result)
	{
		((CompatibleActivity)getActivity()).cancelProgressDialog();
	}
	
	@Override
	public void onRequestFailure(SpiceException se)
	{
		CompatibleActivity activity = (CompatibleActivity)getActivity();
		activity.cancelProgressDialog();
		if(BuildConfig.DEBUG) Log.e(TAG, "Rest error: ", se);
		if(se instanceof RequestCancelledException) return;
		activity.showAlertDialog(R.string.err_title, getString(R.string.err_server));
	}
	
	public boolean hasToolbar()
	{
		return true;
	}
	
	public abstract String getTitle();
	
	protected SpiceManager spiceManager = new SpiceManager(SpiceService.class);
	
	public static final String ARG_PARENT = "parent";
}