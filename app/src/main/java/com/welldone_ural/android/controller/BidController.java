package com.welldone_ural.android.controller;

import android.app.Activity;
import android.app.Fragment;

import android.content.Context;
import android.content.Intent;

import android.os.Build;
import android.os.Bundle;

import android.support.annotation.StringRes;
import android.support.design.widget.FloatingActionButton;

import android.text.SpannableString;
import android.text.style.UnderlineSpan;

import android.util.Log;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.octo.android.robospice.persistence.DurationInMillis;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import com.welldone_ural.android.BuildConfig;
import com.welldone_ural.android.model.Bid;
import com.welldone_ural.android.model.Result;
import com.welldone_ural.android.model.User;
import com.welldone_ural.android.network.ApplyBidRequest;
import com.welldone_ural.android.network.CheckBidRequest;
import com.welldone_ural.android.R;
import com.welldone_ural.android.ui.CompatibleActivity;
import com.welldone_ural.android.ui.MapActivity;

import static com.welldone_ural.android.Utils.TAG;

public class BidController extends BaseController<Result>
{
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		return super.onCreateView(inflater, container, savedInstanceState, R.layout.bid_fragment);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);
		if(savedInstanceState == null) savedInstanceState = getArguments();
		bid = (Bid)savedInstanceState.getParcelable(ARG_BID);
		if(BuildConfig.DEBUG) Log.d(TAG, "Selected Bid: "+bid);
		if(bid != null) title = bid.getName();
		
		TextView addressView = ((TextView)view.findViewById(R.id.bid_address));
		if(bid.getAddress() == null) addressView.setVisibility(View.INVISIBLE);
		else
		{
			SpannableString content = new SpannableString(bid.getAddress());
			content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
			addressView.setText(content);
			addressView.setOnClickListener((View v)->
			{
				Activity activity = getActivity();
				Intent intent = new Intent(activity, MapActivity.class);
				intent.putExtra(ARG_BID, bid);
				activity.startActivity(intent);
				activity.finish();
			});
		}
		
		ImageView avatar = (ImageView)view.findViewById(R.id.avatar);
		Picasso picasso = Picasso.with(getActivity());
		RequestCreator rc;
		if(bid.getUserAvatar() == null) rc = picasso.load(R.drawable.ic_avatar_none);
		else rc = picasso.load(bid.getUserAvatar());
		rc.resize(100, 100).centerCrop().into(avatar);
		
		((TextView)view.findViewById(R.id.user_name)).setText(bid.getUserName());
		if(bid.getPrice() > 0) ((TextView)view.findViewById(R.id.bid_price)).setText(""+bid.getPrice());
		((TextView)view.findViewById(R.id.bid_rate)).setText("Рейтинг: "+bid.getRating());
		((TextView)view.findViewById(R.id.bid_desc)).setText(bid.getDescription());
		((TextView)view.findViewById(R.id.bid_date)).setText("Срок: "+bid.getDate());
		statusView = (TextView)view.findViewById(R.id.tv_status);
		fabApply = (FloatingActionButton)view.findViewById(R.id.fab_apply);
		
		view.findViewById(R.id.fab_comments).setOnClickListener((View v)->
		{
			Bundle args = new Bundle();
			args.putParcelable(ARG_BID, bid);
			CommentsController controller = new CommentsController();
			controller.setArguments(args);
			go(controller, true);
		});
		
		user = User.getUser(getContext());
		if(user != null && user.isExecutor())
		{
			if(BuildConfig.DEBUG) Log.d(TAG, "checking bid status for executor");
			request(new CheckBidRequest(user.getId(), bid.getId()), Result.CACHE_KEY, DurationInMillis.ALWAYS_EXPIRED, true, R.string.check_bid_title);
		}
	}
	
	@Override
	public void onRequestSuccess(Result result)
	{
		super.onRequestSuccess(result);
		if(BuildConfig.DEBUG) Log.d(TAG, "BidController is bid applied: "+result+" bid: "+bid);
		if(result.getIntValue() == 1) statusView.setVisibility(View.VISIBLE);
		else
		{
			fabApply.setVisibility(View.VISIBLE);
			fabApply.setOnClickListener((View v)->
			{
				if(bid.getPrice() > 0)
				{
					if(BuildConfig.DEBUG) Log.d(TAG, "start apply bid");
					request(new ApplyBidRequest(user.getId(), bid.getId(), 0), Result.CACHE_KEY, DurationInMillis.ALWAYS_EXPIRED, true, R.string.check_bid_title);
					return;
				}
				
				Bundle args = new Bundle();
				args.putParcelable(ARG_BID, bid);
				ApplyController controller = new ApplyController();
				controller.setArguments(args);
				go(controller, false);
			});
		}
	}
	
	@Override
	public void onSaveInstanceState(Bundle bundle)
	{
		bundle.putParcelable(ARG_BID, bid);
	}
	
	@Override
	public String getTitle()
	{
		return title;
	}
	
	private String title = "BidController";
	private User user;
	private Bid bid;
	private TextView statusView;
	private FloatingActionButton fabApply;
	
	public static final String ARG_BID = "bid";
}