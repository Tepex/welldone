package com.welldone_ural.android;

import android.app.Activity;

import android.content.Context;
import android.content.Intent;

import android.graphics.Bitmap;

import android.os.Build;
import android.os.Environment;

import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import android.util.Log;

import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;
import com.android.volley.VolleyError;

import com.welldone_ural.android.controller.BaseController;
import com.welldone_ural.android.controller.LastBidsController;
import com.welldone_ural.android.controller.RegistrationController;

import com.welldone_ural.android.ui.MainActivityLP;
import com.welldone_ural.android.ui.MainActivityPre;

import java.lang.reflect.Constructor;

public class Utils
{
	private Utils(Context context)
	{
		requestQueue = Volley.newRequestQueue(context.getApplicationContext());
	}
	
	public <T> void addToRequestQueue(Request<T> req)
	{
		requestQueue.add(req);
	}
	
	public static void displayAvatar(Context ctx, final ImageView imageView, String userAvatar, String vkAvatar)
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "userAvatar: "+userAvatar+" vkAvatar: "+vkAvatar);
		String src;
		if(userAvatar != null) src = userAvatar;
		else if(vkAvatar != null) src = vkAvatar;
		else return;
		
		if(avatarBitmap == null)
		{
			ImageRequest request = new ImageRequest(src, new Response.Listener<Bitmap>()
			{
				@Override
				public void onResponse(Bitmap bitmap)
				{
					updateAvatarCache(bitmap);
					imageView.setImageBitmap(avatarBitmap);
				}
			}, 0, 0, ImageView.ScaleType.CENTER, null, 
			new Response.ErrorListener()
			{
				@Override
				public void onErrorResponse(VolleyError error)
				{
					if(BuildConfig.DEBUG) Log.e(TAG, "volley error: "+error);
				}
			});
			getInstance(ctx).addToRequestQueue(request);
		}
		else
		{
			if(BuildConfig.DEBUG) Log.d(TAG, "Utils. avatar from cache");
			imageView.setImageBitmap(avatarBitmap);
		}
	}
	
	/**
	 * Загруженный в ImageView аватар.
	 */
	public static void updateAvatarCache(Bitmap bitmap)
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "Utils.updateAvatarCache. New bitmap loaded: "+bitmap.getWidth()+"x"+bitmap.getHeight());
		avatarBitmap = bitmap;
	}
	
	public static Bitmap getAvatarBitmap()
	{
		return avatarBitmap;
	}
	
	public static synchronized Utils getInstance(Context ctx)
	{
		if(utils == null) utils = new Utils(ctx);
		return utils;
	}
	
	public static final String TAG = "WellDone";
	
	private static Utils utils;
	private static Context context;
	private static Bitmap avatarBitmap;
	
	private RequestQueue requestQueue;
	
	public static final int ERR_CAST_CONTROLLER = 1;
	public static final int ERR_CONFIGURATION_CHANGED = 2;
	public static final int ERR_VK_NULL = 3;
}


/*
package com.welldone_ural.android.ui;

import android.content.Context;
import android.content.res.Configuration;

import android.os.Bundle;

import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.StringRes;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;

import android.util.Log;

import android.view.MenuItem;
import android.view.View;

import android.widget.TextView;

import com.welldone_ural.android.BuildConfig;
import com.welldone_ural.android.model.User;
import com.welldone_ural.android.R;
import com.welldone_ural.android.Utils;

import static com.welldone_ural.android.Utils.TAG;

public class MainActivityPre extends BaseActivityPre
{
	@CallSuper
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		
		user = User.getUser(this);
		TextView nameView = (TextView)findViewById(R.id.user_name);
		nameView.setText(user.getName());
		TextView balanceView = (TextView)findViewById(R.id.user_balance);
		balanceView.setText(getString(R.string.balance, user.getBalance()));
		
		navigationView = (NavigationView)findViewById(R.id.navigation_view);
		navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener()
		{
			@Override
			public boolean onNavigationItemSelected(MenuItem menuItem)
			{
				if(menuItem.isChecked()) menuItem.setChecked(false);
				else menuItem.setChecked(true);
				drawerLayout.closeDrawers();
				
				if(menuItem.getItemId() == R.id.exit)
				{
					if(BuildConfig.DEBUG) Log.d(TAG, "exit");
					user.setLoggedIn(false);
					user.save(MainActivityPre.this);
					MainActivityPre.this.finish();
					return true;
				}
				else if(true)
				{
					// fragment transaction
					if(BuildConfig.DEBUG) Log.d(TAG, "menu item: "+menuItem.getTitle());
					return true;
				}
				return false;
			}
		});
		
		// Initializing Drawer Layout and ActionBarToggle
		drawerLayout = (DrawerLayout)findViewById(R.id.drawer);
		actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
		{
			@Override
			public void onDrawerClosed(View drawerView)
			{
				super.onDrawerClosed(drawerView);
			}
			
			@Override
			public void onDrawerOpened(View drawerView)
			{
				super.onDrawerOpened(drawerView);
			}
		};
		drawerLayout.setDrawerListener(actionBarDrawerToggle);
		actionBarDrawerToggle.syncState();
	}
	
	@Override
	protected void onPostCreate(Bundle savedInstanceState)
	{
		super.onPostCreate(savedInstanceState);
		actionBarDrawerToggle.syncState();
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig)
	{
		super.onConfigurationChanged(newConfig);
		actionBarDrawerToggle.onConfigurationChanged(newConfig);
	}	
	
	@Override
	protected @LayoutRes int getLayout()
	{
		return R.layout.main_layout;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if(actionBarDrawerToggle.onOptionsItemSelected(item)) return true;
		return super.onOptionsItemSelected(item);
	}	

	/*
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}
 
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
 
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
 
        return super.onOptionsItemSelected(item);
    }
    */

    
    
    /*
    public void goProfile(View view)
    {
    	if(BuildConfig.DEBUG) Log.d(TAG, "goProfile");
		drawerLayout.closeDrawers();
    }
    
    protected User user;
	protected NavigationView navigationView;
	protected DrawerLayout drawerLayout;
	protected ActionBarDrawerToggle actionBarDrawerToggle;
}
*/