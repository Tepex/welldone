package com.welldone_ural.android.network;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.welldone_ural.android.model.User;

public class RegRequest extends RetrofitSpiceRequest<User, RestApi>
{
	public RegRequest(String uname, String usurname, String uemail, String uphone, String upwd)
	{
		super(User.class, RestApi.class);
		name = uname;
		surname = usurname;
		email = uemail;
		phone = uphone;
		pwd = upwd;
	}
	
	@Override
	public User loadDataFromNetwork() throws Exception
	{
		return getService().register(name, surname, email, phone, pwd);
	}
	
	private String name;
	private String surname;
	private String email;
	private String phone;
	private String pwd;
	private String about;
}