package com.welldone_ural.android.network;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.welldone_ural.android.model.Bid;

public class BidRequest extends RetrofitSpiceRequest<Bid[], RestApi>
{
	public BidRequest()
	{
		super(Bid[].class, RestApi.class);
	}
	
	@Override
	public Bid[] loadDataFromNetwork() throws Exception
	{
		return getService().bids();
	}
}