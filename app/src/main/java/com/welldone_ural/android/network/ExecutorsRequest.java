package com.welldone_ural.android.network;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.welldone_ural.android.model.User;

public class ExecutorsRequest extends RetrofitSpiceRequest<User[], RestApi>
{
	public ExecutorsRequest()
	{
		super(User[].class, RestApi.class);
	}
	
	@Override
	public User[] loadDataFromNetwork() throws Exception
	{
		return getService().getExecutors();
	}
}
