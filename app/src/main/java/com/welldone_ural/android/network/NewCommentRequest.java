package com.welldone_ural.android.network;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.welldone_ural.android.model.User;
import com.welldone_ural.android.model.Result;

public class NewCommentRequest extends RetrofitSpiceRequest<Result, RestApi>
{
	public NewCommentRequest(String userEmail, String userPwd, long bidId, String msg)
	{
		super(Result.class, RestApi.class);
		this.userEmail = userEmail;
		this.userPwd = userPwd;
		this.bidId = bidId;
		this.msg = msg;
	}
	
	@Override
	public Result loadDataFromNetwork() throws Exception
	{
		return getService().newComment(userEmail, userPwd, bidId, msg);
	}
	
	private String userEmail;
	private String userPwd;
	private long bidId;
	private String msg;
}