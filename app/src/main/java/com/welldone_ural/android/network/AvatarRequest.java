package com.welldone_ural.android.network;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import com.welldone_ural.android.model.Result;

import retrofit.mime.TypedFile;

import java.io.File;

public class AvatarRequest extends RetrofitSpiceRequest<Result, RestApi>
{
	public AvatarRequest(String email, String pwd, File image)
	{
		super(Result.class, RestApi.class);
		this.email = email;
		this.pwd = pwd;
		file = new TypedFile("image/jpeg", image);
	}
	
	@Override
	public Result loadDataFromNetwork() throws Exception
	{
		return getService().avatar(email, pwd, file);
	}
	
	private String email;
	private String pwd;
	private TypedFile file;
}