package com.welldone_ural.android.network;

import retrofit.RetrofitError;

/**
 * Исключение для REST-методов протокола.
 * Выпадает, если HTTP-код возврата 400, 404 или 500.
 */
public class RestException extends RuntimeException
{
	public RestException(RetrofitError cause)
	{
		super(cause);
	}
}