package com.welldone_ural.android.network;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.welldone_ural.android.model.Bid;

public class BidsInWorkRequest extends RetrofitSpiceRequest<Bid[], RestApi>
{
	public BidsInWorkRequest(String email, String pwd)
	{
		super(Bid[].class, RestApi.class);
		this.email = email;
		this.pwd = pwd;
	}
	
	@Override
	public Bid[] loadDataFromNetwork() throws Exception
	{
		return getService().getBidsInWork(email, pwd);
	}
	
	private String email;
	private String pwd;
}