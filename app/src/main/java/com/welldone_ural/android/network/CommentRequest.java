package com.welldone_ural.android.network;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.welldone_ural.android.model.Comment;

public class CommentRequest extends RetrofitSpiceRequest<Comment[], RestApi>
{
	public CommentRequest(long tid)
	{
		super(Comment[].class, RestApi.class);
		taskId = tid;
	}
	
	@Override
	public Comment[] loadDataFromNetwork() throws Exception
	{
		return getService().getComments(taskId);
	}
	
	private long taskId;
}
