package com.welldone_ural.android.network;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.welldone_ural.android.model.Result;

public class ApplyBidRequest extends RetrofitSpiceRequest<Result, RestApi>
{
	public ApplyBidRequest(long uid, long bid, int p)
	{
		super(Result.class, RestApi.class);
		userId = uid;
		bidId = bid;
		price = p;
	}
	
	@Override
	public Result loadDataFromNetwork() throws Exception
	{
		if(price == 0) return getService().applyBid(userId, bidId);
		return getService().applyBidPrice(userId, bidId, price);
	}
	
	private long userId;
	private long bidId;
	private int price;
}