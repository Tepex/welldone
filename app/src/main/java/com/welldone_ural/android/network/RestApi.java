package com.welldone_ural.android.network;

import retrofit.http.POST;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.Multipart;
import retrofit.http.Part;
import retrofit.http.Query;

import retrofit.mime.TypedFile;

import com.welldone_ural.android.model.Bid;
import com.welldone_ural.android.model.Category;
import com.welldone_ural.android.model.Comment;
import com.welldone_ural.android.model.Result;
import com.welldone_ural.android.model.User;

public interface RestApi
{
	/**
	 * Регистрация пользователя.
	 *
	 * @param name				 имя пользователя.
	 * @param email				 email пользователя.
	 * @param phone				 телефон пользователя.
	 * @param pass				 пароль пользователя.
	 * @exception RestException	 выбрасывается при статус коде HTTP 500
	 * @return					 в случае успеха возвращает id зарегистрированного пользователя.
	 */
	@FormUrlEncoded
	@POST(RESOURCE_REG)
	public User register(
		@Field(PARAM_NAME) String name,
		@Field(PARAM_SURNAME) String surname,
		@Field(PARAM_EMAIL) String email,
		@Field(PARAM_PHONE) String phone,
		@Field(PARAM_PASS) String pass) throws RestException;
	
	/**
	 * Авторизация пользователя.
	 *
	 * @param email				 email пользователя.
	 * @param pass				 пароль пользователя.
	 * @exception RestException	 выбрасывается при статус коде HTTP 500
	 * @return					 в случае успеха возвращает id зарегистрированного пользователя.
	 */
	@FormUrlEncoded
	@POST(RESOURCE_AUTH)
	public User auth(
		@Field(PARAM_EMAIL) String email,
		@Field(PARAM_PASS) String pass) throws RestException;
	
	
	/**
	 * Загрузка списка категорий.
	 *
	 * @exception RestException	 выбрасывается при статус коде HTTP 500
	 * @return					 в случае успеха возвращает список категорий.
	 */
	@POST(RESOURCE_CATEGORIES)
	public Category[] categories() throws RestException;

	/**
	 * Загрузка списка заданий.
	 *
	 * @exception RestException	 выбрасывается при статус коде HTTP 500
	 * @return					 в случае успеха возвращает список заданий.
	 */
	@POST(RESOURCE_BIDS)
	public Bid[] bids() throws RestException;

	/**
	 * Загрузка аватарки.
	 *
	 * @param email				 email пользователя.
	 * @param pass				 пароль пользователя.
	 * @param avatar			 аватарка
	 * @exception RestException	 выбрасывается при статус коде HTTP 500
	 * @return					 в случае успеха возвращает имя файла для скачивания.
	 */
	@Multipart
	@POST(RESOURCE_AVATAR)
	public Result avatar(
		@Part(PARAM_EMAIL) String email,
		@Part(PARAM_PASS) String pass,
		@Part(PARAM_AVATAR) TypedFile file) throws RestException;
	
	@FormUrlEncoded
	@POST(CHECK_BID)
	public Result checkBid(
		@Field(PARAM_USER_ID) long userId,
		@Field(PARAM_BID_ID) long bidId) throws RestException;
	
	@FormUrlEncoded
	@POST(APPLY_BID)
	public Result applyBid(
		@Field(PARAM_USER_ID) long userId,
		@Field(PARAM_BID_ID) long bidId) throws RestException;

	@FormUrlEncoded
	@POST(APPLY_BID)
	public Result applyBidPrice(
		@Field(PARAM_USER_ID) long userId,
		@Field(PARAM_BID_ID) long bidId,
		@Field(PARAM_MY_PRICE) int price) throws RestException;
	
	@FormUrlEncoded
	@POST(COMMENTS)
	public Comment[] getComments(
		@Field(PARAM_BID_ID) long bidId
		) throws RestException;
	
	@FormUrlEncoded
	@POST(COMMENT)
	public Result newComment(
		@Field(PARAM_USER_EMAIL) String userEmail,
		@Field(PARAM_USER_PWD) String userPwd,
		@Field(PARAM_BID_ID1) long taskId,
		@Field(PARAM_COMMENT) String msg
		) throws RestException;
	
	/**
	 * Список заданий на которых был отклик
	 */
	@FormUrlEncoded
	@POST(MYINWAIT)
	public Bid[] getBidsInWait(
		@Field(PARAM_USER_EMAIL) String userEmail,
		@Field(PARAM_USER_PWD) String userPwd
		) throws RestException;

	/**
	 * Список завершенных заданий
	 */
	@FormUrlEncoded
	@POST(MYINCOMPLETE)
	public Bid[] getBidsIncomplete(
		@Field(PARAM_USER_EMAIL) String userEmail,
		@Field(PARAM_USER_PWD) String userPwd
		) throws RestException;
	
	/**
	 * Список заданий в работе
	 */
	@FormUrlEncoded
	@POST(MYINWORK)
	public Bid[] getBidsInWork(
		@Field(PARAM_USER_EMAIL) String userEmail,
		@Field(PARAM_USER_PWD) String userPwd
		) throws RestException;

	/**
	 * Получение списка исполнителей.
	 */
	@POST(RESOURCE_EXECUTORS)
	public User[] getExecutors() throws RestException;
	
	public static final String RESOURCE_REG = "/user/user_reg/";
	public static final String RESOURCE_AUTH = "/user/auth/";
	public static final String RESOURCE_AVATAR = "/user/avatar/";
	public static final String RESOURCE_CATEGORIES = "/category/";
	public static final String RESOURCE_BIDS = "/bids/";
	public static final String RESOURCE_EXECUTORS = "/executors/";
	public static final String CHECK_BID = "/task/reviewtoperformer/";
	public static final String APPLY_BID = "/task/apply/";
	public static final String COMMENTS = "/task/comments/";
	public static final String COMMENT = "/task/comment/";
	
	public static final String MYINWAIT = "/task/myinwait/";
	public static final String MYINCOMPLETE = "/task/myincomplete/";
	public static final String MYINWORK = "/task/myinwork/";
	
	public static final String PARAM_NAME = "name";
	public static final String PARAM_SURNAME = "surname";
	//public static final String PARAM_EMAIL = "userEmail";
	public static final String PARAM_EMAIL = "email";
	public static final String PARAM_PHONE = "phone";
	//public static final String PARAM_PASS = "userPassword";
	public static final String PARAM_PASS = "pass";
	
	public static final String PARAM_ABOUT = "about";
	public static final String PARAM_AVATAR = "avatar";
	public static final String PARAM_USER_ID = "USER-ID";
	public static final String PARAM_BID_ID = "task";
	public static final String PARAM_MY_PRICE = "myprice";
	public static final String PARAM_USER_EMAIL = "userEmail";
	public static final String PARAM_USER_PWD = "userPassword";
	public static final String PARAM_BID_ID1 = "taskID";
	public static final String PARAM_COMMENT = "msg";
	
}
