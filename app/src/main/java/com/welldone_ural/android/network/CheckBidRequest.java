package com.welldone_ural.android.network;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.welldone_ural.android.model.Result;

public class CheckBidRequest extends RetrofitSpiceRequest<Result, RestApi>
{
	public CheckBidRequest(long uid, long bid)
	{
		super(Result.class, RestApi.class);
		userId = uid;
		bidId = bid;
	}
	
	@Override
	public Result loadDataFromNetwork() throws Exception
	{
		return getService().checkBid(userId, bidId);
	}
	
	private long userId;
	private long bidId;
}