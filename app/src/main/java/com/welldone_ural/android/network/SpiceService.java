package com.welldone_ural.android.network;

import com.octo.android.robospice.retrofit.RetrofitGsonSpiceService;
import retrofit.RestAdapter;

public class SpiceService extends RetrofitGsonSpiceService
{
	@Override
	public void onCreate()
	{
		super.onCreate();
		addRetrofitInterface(RestApi.class);
	}
	
	@Override
	protected RestAdapter.Builder createRestAdapterBuilder()
	{
		return Rest.getRestBuilder(getServerUrl());
	}
	
	@Override
	protected String getServerUrl()
	{
		return Rest.MAIN;
	}
}