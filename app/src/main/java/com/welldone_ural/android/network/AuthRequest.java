package com.welldone_ural.android.network;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.welldone_ural.android.model.User;

public class AuthRequest extends RetrofitSpiceRequest<User, RestApi>
{
	public AuthRequest(String login, String pwd)
	{
		super(User.class, RestApi.class);
		this.login = login;
		this.pwd = pwd;
	}
	
	@Override
	public User loadDataFromNetwork() throws Exception
	{
		return getService().auth(login, pwd);
	}
	
	private String login;
	private String pwd;
}