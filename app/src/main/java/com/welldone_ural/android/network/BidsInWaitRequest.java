package com.welldone_ural.android.network;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.welldone_ural.android.model.Bid;

public class BidsInWaitRequest extends RetrofitSpiceRequest<Bid[], RestApi>
{
	public BidsInWaitRequest(String email, String pwd)
	{
		super(Bid[].class, RestApi.class);
		this.email = email;
		this.pwd = pwd;
	}
	
	@Override
	public Bid[] loadDataFromNetwork() throws Exception
	{
		return getService().getBidsInWait(email, pwd);
	}
	
	private String email;
	private String pwd;
}