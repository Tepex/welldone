package com.welldone_ural.android.network;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.welldone_ural.android.model.Category;

public class CategoryRequest extends RetrofitSpiceRequest<Category[], RestApi>
{
	public CategoryRequest()
	{
		super(Category[].class, RestApi.class);
	}
	
	@Override
	public Category[] loadDataFromNetwork() throws Exception
	{
		return getService().categories();
	}
}